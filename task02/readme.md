Task: write a Guess the Number game using the Bash script.
	Subtask 1: create a random number generator script.
	1. Create script file game.sh commit 93410ad
	2. Generate and display random number commit fbb8799

	Subtask 1 completed. commit 56eaf19

	Subtask 2: Extract Function refactoring.

	1. Add function for generate number
	Subtask 2 completed.

	Subtask 3: Compare the randomly generated number X with the number Y, which is given by the first parameter of the string and display the corresponding message.
	
	1. Add input from user (num), compare num with num random
	Subtask 3 completed.

	Subtask 4: Implement number input if number is not specified in command parameter

	1. Add interactive number input
	Subtask 4 completed.

	Subtask 5: Implement the ability to set the value of the upper limit of the range of random numbers by the second command line parameter as an integer from 1 to 100.

	1. Add ability to set the upper limit by command line param
	Subtask 5 completed.

	Subtask 6: Implement prompted upper bound value input if no second command line parameter is specified.

	1. Move to function: set upper bound
	2. Add menu: set upper bound
	3. Add params: min bottom bound and max upper bound
	4. Add menu invoke if second command line param is empty
	Subtask 6 completed.

	Subtask 7: Implement the ability to specify the number of guessing attempts by the third command line parameter.

	1. Move number generation and comparisons to loop
	2. Add: set number of retries by command line param
	Subtask 7 completed.

	Subtask 8: Implement prompt input for the number of guess attempts if the third command line parameter is not specified.

	1. Move to function: set number of retries
	2. Add menu: set number of retries
	3. Add menu invoke if second command line param is empty
	Subtask 8 completed.

	Subtask 9: Implement prompt input for the number of guess attempts if the third command line parameter is not specified.

	Refactoring:

	1. Move to function: set user number; menu set user number
	2. Add: set params by keys.
	Refactoring completed.

	Subtask 10: Implement the possibility of replaying the game with the same parameters after successfully guessing the number.

	1. Add main menu
	2. Add play menu
	3. Move to function: compare generated and user nums
	Subtask completed.

	Fix: delete unused  default params  setting
	Refactoring: add: setting default params by flags
