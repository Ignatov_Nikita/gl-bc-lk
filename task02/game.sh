#!/bin/bash

#takes two num: bottomBound and upperBound
function generateNum()
{
	if [[ $1 > $2 ]]
	then
		return 1
	fi
	range=$(($2-$1 +1))
	number=$((($RANDOM % $range) + $1))
}

function isNumber()
{
	return $([[ $1 =~ ^[[:digit:]]+$ ]])
}

function setUserNum()
{
	#if not num
	if ! isNumber $1
	then
		return 2
	fi
	userNum=$1
}

function menuSetUserNum
{
	local ans
	local res=1

	#exit when command return is success
	until [[ res -eq 0 ]]
	do
		read -p "Please, enter num [$bottomBound-$upperBound]: " ans
		setUserNum $ans
		res=$?
	done
}

function setUpperBound()
{
	#if not num
	if ! isNumber $1
	then
		return 2
	fi
	if [[ $1 -ge $minBottomBound && $1 -le $maxUpperBound ]]
	then
		upperBound=$1
	else
		return 1
	fi
}

function menuSetUpperBound
{
	local ans
	local res=1

	#exit when command return is success
	until [[ res -eq 0 ]]
	do
		read -p "Please, enter upper bound [$minBottomBound-$maxUpperBound] " ans
		setUpperBound $ans
		res=$?
	done
}

function setNumRetries()
{
	#if not num
	if ! isNumber $1
	then
		return 2
	fi
	if [[ $1 -ge 1 ]]
	then
		numRetries=$1
	else
		return 1
	fi
}

function menuSetNumRetries
{
	local ans
	local res=1

	#exit when command return is success
	until [[ res -eq 0 ]]
	do
		read -p "Please, enter num retries [>0]: " ans
		setNumRetries $ans
		res=$?
	done
}

function play
{

	if [[ $userNum -gt $number ]]
	then
		return 2
	elif [[ $userNum -lt $number ]]
	then
		return 1
	elif [[ $userNum -eq $number ]]
	then
		return 0
	fi
	return 3
}

function menuPlay
{
	local res
	local numRetriesCur=$numRetries
	while [[ $numRetriesCur -gt 0 ]]
	do
		if [[ -z $userNum ]]
		then
			menuSetUserNum
		fi
		generateNum $bottomBound $upperBound
		play
		res=$?
		echo "res: $res"

		if [[ $res -eq 2 ]]
		then
			echo "Your number $userNum is greater than $number, range [$bottomBound-$upperBound]"
		elif [[ $res -eq 1 ]]
		then
			echo "Your number $userNum is less than $number, range [$bottomBound-$upperBound]"
		elif [[ $res -eq 0 ]]
		then
			echo "Your number $userNum is equal to $number, range [$bottomBound-$upperBound]"
			echo
			echo "Your win!"
			unset userNum
			return 0
		fi
		let numRetriesCur-=1
		unset userNum
	done
}

#set default params
	minBottomBound=1
	maxUpperBound=100
	bottomBoundDefault=0
	upperBoundDefault=8
	numRetriesDefault=3


#set settings by flags
posArgs=()
while [[ $# -ne 0 ]]
do
	case "$1" in

		'-u' | '--upper')
			setUpperBound $2
			shift
		;;
		
		'-r' | '--retries')
			setNumRetries $2
			shift
		;;

		'-d' | '--default')
			upperBound=$upperBoundDefault
			numRetries=$numRetriesDefault
		;;

		*)
			posArgs+=($1)
		;;
	esac
	shift
done

#set settings to default/posArg/input
bottomBound=$bottomBoundDefault
if [[ -z $upperBound ]]
then
	if [[ -z ${posArgs[1]} ]]
	then
		menuSetUpperBound
	else
		setUpperBound ${posArgs[1]}
	fi
fi
if [[ -z $numRetries ]]
then
	if [[ -z ${posArgs[2]} ]]
	then
		menuSetNumRetries
	else
		setNumRetries ${posArgs[2]}
	fi
fi
if [[ -z $userNum ]]
then
	if [[ -z ${posArgs[0]} ]]
	then
		menuSetUserNum
	else
		setUserNum ${posArgs[0]}
	fi
fi
unset posArgs

#main menu
while [[ 0 ]]
do
	echo
	select var in 'Play' 'Parameters' 'Exit'
	do
		echo
		case $var in
			'Play')
				menuPlay
			;;

			'Parameters')
				select var in 'Back' 'NumRetries' 'UpperBound'
				do
					echo
					case $var in
						'Back')
							break
						;;

						'NumRetries')
							menuSetNumRetries
						;;

						'UpperBound')
							menuSetUpperBound
						;;
					esac
				done
			;;

			'Exit')
				exit 0
			;;
		esac
		break
	done
done