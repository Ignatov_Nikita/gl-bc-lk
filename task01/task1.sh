#!/bin/bash

#takes one number 0-1
#0-partial format
#1-full format
function setDspFullFormat()
{
	if [[ $1 -eq 1 ]]
	then
		lsFullFormat=" -l"
	elif [[ $1 -eq 0 ]]
	then
		lsFullFormat=
	else
		return 1
	fi
}

#takes one number 0-1
#0-hide hidden element
#1-show hidden element
function setDspShowHiddenEl()
{
	if [[ $1 -eq 1 ]]
	then
		lsHideEl=" -a"
	elif [[ $1 -eq 0 ]]
	then
		lsHideEl=
	else
		return 1
	fi
}

#takes one char [hsS]
#h-hide dirs
#s-show dirs
#S-show only dirs
function setDspDirs()
{
	case "$1" in
		'h')
			lsFilter=1
			showDirs=" -v /"
		;;

		's')
			lsFilter=0
			showDirs=
		;;
		
		'S')
			lsFilter=1	
			showDirs=" /"
		;;

		*)
			return 1
		;;	
	esac
}

#takes one number 1-2
#1-ascending order
#1-descending order
function setOrder()
{
	if [[ $1 -eq 1 ]]
	then
		lsOrder=
	elif [[ $1 -eq 2 ]]
	then
		lsOrder=" -r"
	else
		return 1
	fi
}

#takes one char [nstx]
#n-by name
#s-by size
#t-by modified time
#x-by extension
function setSort()
{
	case "$1" in
		'n')
			lsSort=
		;;
		's')
			lsSort=" -S"
		;;
		't')
			lsSort=" -t"
		;;
		'x')
			lsSort=" -X"
		;;

		*)
			return 1
		;;
	esac
}

function menuDspMod
{

while [[ 0 ]]
do
	echo
	select var in \
	'Back' 'Full/Partial format' 'Show/hide hidden el' \
	'Show all' 'Hide dirs' 'Dsp only dirs' 'Sort'
	do
		case "$var" in

		'Full/Partial format')
			if [ -n "$lsFullFormat" ]
			then
				setDspFullFormat 0
			else
				setDspFullFormat 1
			fi
		;;

		'Show/hide hidden el')
			if [ -n "$lsHideEl" ]
			then
				setDspShowHiddenEl 0
			else
				setDspShowHiddenEl 1
			fi
		;;

		'Show all')
			setDspDirs 's'
		;;

		'Hide dirs')
			setDspDirs 'h'
		;;

		'Dsp only dirs')
			setDspDirs 'S'
		;;

		'Sort')
		while [[ 0 ]]
		do
			echo
			select var2 in \
			'Back' 'By Name' 'By Size' \
			'By TimeMod' 'By Extension' 'ASC/DESC'
			do
				case "$var2" in
				'Back')
					break 2
				;;

				'By Name')
					setSort 'n'
				;;

				'By Size')
					setSort 's'
				;;

				'By TimeMod')
					setSort 't'
				;;

				'By Extension')
					setSort 'x'
				;;

				'ASC/DESC')
					if [ -n "$lsOrder" ]
					then
						setOrder 1
					else
						setOrder 2
					fi
				;;

				*)
				echo "Unknown command: $REPLY"
				;;
				esac
			break
			done
		done
		;;		

		'Back')
			return 0
		;;

		*)
			echo "Unknown command: $1"
		;;
		esac	
	break
	done
done
}

#takes name dir. if empty, get from console
function changeDir()
{
	local ans
	if [[ -z "$1" ]]
	then
		read -p "[Cd] Name dir: " ans
	else
		ans="$1"
	fi
	cd "$ans"
}

#takes source and destination. if any of that empty, get from console
function copy()
{
	local ans1
	local ans2
	if [[ -z "$1" || -z "$2" ]]
	then
		read -p "[Cp] Name source: " ans1 
		read -p "[Cp] Name destination: " ans2
	else
		ans1="$1"
		ans2="$2"
	fi
	cp "$ans1" "$ans2"
}

#takes source and destination. if any of that empty, get from console		
function move()
{
	local ans1
	local ans2
	if [[ -z "$1" || -z "$2" ]]
	then
		read -p "[Mv] Name source: " ans1
		read -p "[Mv] Name destination: " ans2
	else
		ans1="$1"
		ans2="$2"
	fi
	mv "$ans1" "$ans2"
}

#takes name file. if empty, get from console
function delete()
{
	local ans
	if [[ -z "$1" ]]
	then
		read -p "[Del] Name file: " ans
	else
		ans="$1"
	fi
	rm "$ans"
}

#set default parameters
filterProg="grep --no-messages --color=never"
lsFullFormat=
lsHideEl=
lsSort=
lsOrder=
PS3="Select num: "

#processing command line parameters
while [[ $# -ne 0 ]]
do
	case "$1" in

		'-h' | '--help')
			printf "Help:\n"
			printf "\t-d set directory\n\n"
			printf "\t-D=param\n"
			printf "\t\tset directory display mode\n"
			printf "\th-hide dirs s-show all S-show only dirs\n\n"
			printf "\t-a show hidden elements\n\n"
			printf "\t-l set full format display\n\n"
		exit 0
		;;
		
		'-d') #set dir
			changeDir "$2"
			shift
		;;

		'-l') #full display format
			setDspFullFormat 1
		;;

		'-a') #show hidden element
			setDspShowHiddenEl 1
		;;

		-D=*) #dirs display mode
			setDspDirs "${1#*=}"
		;;

		-s=*) #sort mode
			setSort "${1#*=}"
		;;

		'-r') #reverse order
			setOrder 2
		;;

		*)
		echo "Unknown command: $1"
		;;
	esac
	shift
done



while [[ 0 ]]
do
#set display options for ls
lsOptsDsp="${lsFullFormat}${lsHideEl}${lsSort}${lsOrder}"

#content display
printf "Content of $PWD:\n"

if [[ lsFilter -eq 0 ]]
then

	ls -p --color${lsOptsDsp}
else
	ls -p --color${lsOptsDsp} | ${filterProg}${showDirs}
fi

#main menu
	echo
	select var in \
	'DspMod' 'Change_dir' 'Copy' 'Move/Rename' 'Delete' 'Exit'
	do
		case "$var" in

		'DspMod')
		menuDspMod;;

		'Change_dir')
		changeDir;;

		'Copy')
		copy;;

		'Move/Rename')
		move;;

		'Delete')
		delete;;

		'Exit')
		exit 0;;

		*)
		echo "Unknown command: $REPLY";;
		esac
	break
	done
echo
done

