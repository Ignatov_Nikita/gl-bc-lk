#!/bin/bash

renamed=

res=$(find "$1" -type f -mtime +30 | xargs)

if [[ -z $res ]]
then
	echo "No files older than 30 days"
	exit 1
else
	echo "Files older than 30 days:"
	printf "${res}\n"
fi

for name in $res
do
	mv "$name" "$(dirname $name)/~$(basename $name)"
	if [[ $? -eq 0 ]]
	then
		renamed=1
	fi
done

if [[ renamed -eq 1 ]]
then
	../exercise01/ex1.sh ./
fi

