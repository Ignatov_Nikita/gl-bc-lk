#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <asm/uaccess.h>

MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Oleg Tsiliuric <olej@front.ru>" );
MODULE_VERSION( "6.3" );
#define BUFFER_SIZE (1024 * 1024)

//static char str[] = "Hello";
static char buffer[BUFFER_SIZE] = "HI";
static size_t msg_start = 0;
static size_t msg_end = 0;

static ssize_t dev_read( struct file * file, char * buf,
                           size_t count, loff_t *ppos )
{
//	size_t msg_length;
	size_t left;
	size_t start;
   printk( KERN_INFO "=== read : %ld\n", (long)count );
   if( *ppos != 0 )
   {
     printk( KERN_INFO "=== read return : 0\n" );  // EOF
      return 0;
   }
   //if(msg_end != 0)
//   if (count > (msg_end - msg_start))
//		count = (msg_end - msg_start);
   if (count + *ppos > msg_end)
   {
		count = msg_end - *ppos;
		start = msg_end - count;
	}
	else
	start = *ppos;
if(count == 0)
	return 0;
   left = raw_copy_to_user( buf, buffer + start, count );
   //left = raw_copy_to_user( buf, str, len );
   *ppos += count - left;

 //   msg_length = msg_length - left;
 //   msg_pos = msg_length - left;

    if (left)
    {
		printk(KERN_ERR "failed to read %u from %u chars\n", left, count);
		return -EINVAL;
    }
    else
		printk(KERN_NOTICE "read %u chars\n", count);
   
   printk( KERN_INFO "=== read return : %d\n", count );
   return count;
   //*/
}

static ssize_t dev_write( struct file * file, const char * buf,
                           size_t count, loff_t *ppos )
{
	size_t msg_length;
	size_t left;
   printk( KERN_INFO "=== write : %ld\n", (long)count );
 //  if( count < len ) return -EINVAL;
 //  if( *ppos != 0 ) {
  //    printk( KERN_INFO "=== read return : 0\n" );  // EOF
 //     return 0;
 //  }
	if(count > BUFFER_SIZE - msg_end)
	{
		printk(KERN_WARNING "reduse message length from %u to %u chars\n", count, BUFFER_SIZE);
		msg_length = BUFFER_SIZE - msg_end;
	}
	else
        msg_length = count;
        
   left = raw_copy_from_user( buffer + msg_end, buf, msg_length );
   *ppos += msg_length - left;

    msg_start = msg_end + 1;
    msg_end += msg_length - left;

    if (left)
    {
        printk(KERN_ERR "failed to write %u from %u chars\n", left, msg_length);
		return -EINVAL;
    }
    else
        printk(KERN_NOTICE "written %u chars\n", msg_length);
   
   printk( KERN_INFO "=== write return : %d\n", msg_length );
   return msg_length;
}
   
static int __init dev_init( void );
module_init( dev_init );

static void __exit dev_exit( void );
module_exit( dev_exit );

