/*
 * time.c
 *
 *  Created on: Jan 20, 2021
 *      Author: nikitasinys
 */
#include "time.h"

uint16_t get_hour(uint64_t second)
{
	return (second / 60 / 60);
}

//max return value is 59
uint16_t get_cur_min(uint64_t second)
{
	return (second / 60) % 60;
}

//max return value is 59
uint16_t get_cur_sec(uint64_t second)
{
	return second % 60;
}

//time in format HH:MM:SS
//	hours are set to 00 after 99
char * get_time(uint64_t second, char buffer [9])
{
	uint16_t temp;

	if((temp = get_hour(second) % 100) < 10) // % 100 for max value is 99
	{
		buffer[0] = '0';
		itoa(temp, buffer+1, 10);
	}
	else
	{
		itoa(temp, buffer, 10);
	}

	buffer[2] = ':'; //should be here or below (above itoa was set to \0)

	if((temp = get_cur_min(second)) < 10)
	{
		buffer[3] = '0';
		itoa(temp, buffer+4, 10);
	}
	else
	{
		itoa(temp, buffer+3, 10);
	}

	buffer[5] = ':'; //should be here or below (above itoa was set to \0)

	if((temp = get_cur_sec(second)) < 10)
	{
		buffer[6] = '0';
		itoa(temp, buffer+7, 10);
	}
	else
	{
		itoa(temp, buffer+6, 10); //was set \0 at the end of the buffer
	}
	return buffer;
}


void start_stopwatch(Stopwatch * stopwatch)
{
	if(!stopwatch->cur_time) //check to null pointer
		return;
	stopwatch->start_time = *stopwatch->cur_time;
	stopwatch->switched_on = true;
}

void stop_stopwatch(Stopwatch * stopwatch)
{
	if(!stopwatch->cur_time) //check to null pointer
			return;
	stopwatch->duration += *stopwatch->cur_time - stopwatch->start_time;
	stopwatch->switched_on = false;
}

void reset_stopwatch(Stopwatch * stopwatch)
{
	stopwatch->duration = 0;
	stopwatch->switched_on = false;
}

void toggle_stopwatch(Stopwatch * stopwatch)
{
	if(stopwatch->switched_on)
	{
		stop_stopwatch(stopwatch);
	}
	else
	{
		start_stopwatch(stopwatch);
	}
}

uint64_t stopwatch_time(Stopwatch * stopwatch)
{
	if(!stopwatch->cur_time) //check to null pointer
			return 0;
	if(stopwatch->switched_on)
	{
		return *stopwatch->cur_time - stopwatch->start_time + stopwatch->duration;
	}
	else
	{
		return stopwatch->duration;
	}
}
