/*
 * button_event.c
 *
 *  Created on: Jan 24, 2021
 *      Author: nikitasinys
 */

#include "button_event.h"


bool is_button_pressed(int8_t _button_state)
{
	if(_button_state == BPressed)
	{
		HAL_Delay(50);
		if(_button_state == BPressed)
		{
			return true;
		}
	}
	return false;
}

bool is_button_released(int8_t _button_state)
{
	if(_button_state == BReleased)
	{
		HAL_Delay(50);
		if(_button_state == BReleased)
		{
			return true;
		}
	}
	return false;
}

bool is_button_clicked(int8_t _button_state)
{
	static int8_t pressed = false;
	if(is_button_pressed(_button_state))
	{
		pressed = true;
	}
	if(pressed && is_button_released(_button_state))
	{
		pressed = false;
		return true;
	}
	return false;
}

bool is_button_press_and_hold(int8_t _button_state, uint16_t millisec_hold)
{
	static int32_t time_press = -1;

	if(is_button_pressed(_button_state))
	{
		if(time_press == -1) //timer not set
		{
			time_press = HAL_GetTick();
			return false;
		}
	}
	else //button released
	{
		time_press = -1;
		return false;
	}

	if(HAL_GetTick() - time_press >= millisec_hold)
	{
		return true;
	}
	return false;
}
