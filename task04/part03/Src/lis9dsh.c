/*
 * lis9dsh.c
 *
 *  Created on: Feb 17, 2021
 *      Author: nikitasinys
 */


#include "lis9dsh.h"


void ACLMT_Init(LIS9DSH * dev)
{
	uint8_t i2cbuffer[16];

	i2cbuffer[0]=0x40;
	i2cbuffer[1]=0x38;
	i2cbuffer[2]=0x41;
	i2cbuffer[3]=0x38;
	i2cbuffer[4]=0x38;
	i2cbuffer[5]=0x00;

	HAL_I2C_Mem_Write(dev->hi2c, dev->addr_write, 0x10, I2C_MEMADD_SIZE_8BIT, i2cbuffer, 1, 1000);
	HAL_I2C_Mem_Write(dev->hi2c, dev->addr_write, 0x1f, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 1, 1, 1000);
	HAL_I2C_Mem_Write(dev->hi2c, dev->addr_write, 0x12, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 2, 1, 1000);
	HAL_I2C_Mem_Write(dev->hi2c, dev->addr_write, 0x20, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 3, 1, 1000);
	HAL_I2C_Mem_Write(dev->hi2c, dev->addr_write, 0x1e, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 4, 1, 1000);
	HAL_I2C_Mem_Write(dev->hi2c, dev->addr_write, 0x21, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 5, 1, 1000);

	//	res = HAL_I2C_Mem_Read(&hi2c1, aclmt1_r, 0x10, I2C_MEMADD_SIZE_8BIT, i2cbuffer, 1, 1000);
	//	res = HAL_I2C_Mem_Read(&hi2c1, aclmt1_r, 0x1f, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 1, 1, 1000);
	//	res = HAL_I2C_Mem_Read(&hi2c1, aclmt1_r, 0x12, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 2, 1, 1000);
	//	res = HAL_I2C_Mem_Read(&hi2c1, aclmt1_r, 0x20, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 3, 1, 1000);
	//	res = HAL_I2C_Mem_Read(&hi2c1, aclmt1_r, 0x1e, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 4, 1, 1000);
	//	res = HAL_I2C_Mem_Read(&hi2c1, aclmt1_r, 0x21, I2C_MEMADD_SIZE_8BIT, i2cbuffer + 5, 1, 1000);

}

int8_t ReadRaw(LIS9DSH * dev, uint16_t mem_address, uint8_t * buffer, uint16_t count)
{
	int8_t res;

	if(dev == NULL || buffer == NULL)
		return -1;

	res = HAL_I2C_Mem_Read(dev->hi2c, dev->addr_read, mem_address,
					I2C_MEMADD_SIZE_8BIT, buffer, count, 1000);

	if(res != 0)
		return -2;

	return 0;
}

int8_t ReadRawData(LIS9DSH * dev, int16_t (*xyz)[3])
{
	int8_t res;
	uint8_t buffer[6];

	if(dev == NULL || xyz == NULL || *xyz == NULL)
		return -1;

	res = HAL_I2C_Mem_Read(dev->hi2c, dev->addr_read, dev->mem_addr_data,
					I2C_MEMADD_SIZE_8BIT, buffer, 6, 1000);

	if(res != 0)
		return -2;

	(*xyz)[X_axis] = *(int8_t *)(buffer + 1) << 8 | *(buffer);
	(*xyz)[Y_axis] = *(int8_t *)(buffer + 3) << 8 | *(buffer + 2);
	(*xyz)[Z_axis] = *(int8_t *)(buffer + 5) << 8 | *(buffer + 4);

	return 0;
}

int8_t ReadXYZ(LIS9DSH * dev)
{
	int8_t res;

	if(dev == NULL)
		return -1;

	res = ReadRawData(dev, &dev->curXYZ);

	if(res != 0)
		return -2;

	dev->curXYZ[X_axis] -= dev->absXYZ[X_axis];
	dev->curXYZ[Y_axis] -= dev->absXYZ[Y_axis];
	dev->curXYZ[Z_axis] -= dev->absXYZ[Z_axis];

	return 0;
}

int8_t calibrate(LIS9DSH * dev)
{
	int8_t res;

	if(dev == NULL)
		return -1;

	res = ReadRawData(dev, &(dev->absXYZ));
	if(res != 0)
		return -2;

	dev->absXYZ[Z_axis] -= (int16_t)(1.0 / (dev->scale / 32768));

	return 0;
}

float getPitchX(LIS9DSH * dev)
{
	if(dev == NULL)
		return 0;

	return atan2(-dev->curXYZ[X_axis],
					sqrt(dev->curXYZ[Y_axis] * dev->curXYZ[Y_axis]
						 + dev->curXYZ[Z_axis] * dev->curXYZ[Z_axis])
				) * 180 / 3.14;
}
