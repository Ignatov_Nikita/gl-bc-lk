/*
 * lis9dsh.h
 *
 *  Created on: Feb 17, 2021
 *      Author: nikitasinys
 */

#ifndef LIS9DSH_H_
#define LIS9DSH_H_

#include "stm32f4xx_hal.h"

#include <math.h>

typedef struct
{
	I2C_HandleTypeDef * hi2c;
	uint8_t addr_read;
	uint8_t addr_write;
	uint16_t mem_addr_data;
	float scale;
	int16_t absXYZ[3];
	int16_t curXYZ[3];
	int16_t offXYZ[3];
} LIS9DSH;
enum {X_axis, Y_axis, Z_axis};

void ACLMT_Init(LIS9DSH * dev);

int8_t ReadRaw(LIS9DSH * dev, uint16_t mem_address, uint8_t * buffer, uint16_t count);

int8_t ReadRawData(LIS9DSH * dev, int16_t (*xyz)[3]);

int8_t ReadXYZ(LIS9DSH * dev);

int8_t calibrate(LIS9DSH * dev);

float getPitchX(LIS9DSH * dev);


#endif /* LIS9DSH_H_ */
