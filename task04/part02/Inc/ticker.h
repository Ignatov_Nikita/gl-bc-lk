/*
 * ticker.h
 *
 *  Created on: Jan 22, 2021
 *      Author: nikitasinys
 */

#ifndef TICKER_H_
#define TICKER_H_


#include <stdint.h>
#include <string.h>

typedef struct
{
	char * cur_symb;
	char * string;
} Ticker_Message;

typedef struct
{
	Ticker_Message * cur_message;
	Ticker_Message * strings;
	uint16_t cur_string_idx;
	uint16_t strings_count;
	Ticker_Message * delimiter;
	uint16_t display_interval;

} Ticker;

char * ticker_string_pop(Ticker * ticker, char * str_buffer, uint16_t length);

Ticker_Message ticker_msg_create(char * message);

Ticker ticker_create(Ticker_Message * messages, uint16_t msg_count,
		Ticker_Message * delimiter, uint16_t display_interval);

#endif /* TICKER_H_ */
