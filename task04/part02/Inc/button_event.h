/*
 * button_event.h
 *
 *  Created on: Jan 24, 2021
 *      Author: nikitasinys
 */

#ifndef BUTTON_EVENT_H_
#define BUTTON_EVENT_H_

#define BPressed 1
#define BReleased 0

#include "stm32f4xx_hal.h"
#include <stdint.h>
#include <stdbool.h>

bool is_button_pressed(int8_t _button_state);
bool is_button_released(int8_t _button_state);
bool is_button_clicked(int8_t _button_state);
bool is_button_press_and_hold(int8_t _button_state, uint16_t millisec_hold);

#endif /* BUTTON_EVENT_H_ */
