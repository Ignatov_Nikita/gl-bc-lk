
#ifndef __TIME_H
#define __TIME_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct
{
	uint64_t start_time;
	uint64_t duration;
	bool switched_on;
	uint64_t * cur_time;
} Stopwatch;

uint16_t get_hour(uint64_t second);

//max return value is 59
uint16_t get_cur_min(uint64_t second);

//max return value is 59
uint16_t get_cur_sec(uint64_t second);

//time in format HH:MM:SS
//	hours are set to 00 after 99
char * get_time(uint64_t second, char buffer [9]);

void start_stopwatch(Stopwatch * stopwatch);

void stop_stopwatch(Stopwatch * stopwatch);

void reset_stopwatch(Stopwatch * stopwatch);

void toggle_stopwatch(Stopwatch * stopwatch);

uint64_t stopwatch_time(Stopwatch * stopwatch);

#endif /* __TIME_H */
