/*
 * ticker.c
 *
 *  Created on: Jan 22, 2021
 *      Author: nikitasinys
 */

#include "ticker.h"

static Ticker_Message * next_ticker(Ticker * ticker);
static Ticker_Message * next_string(Ticker * ticker);
static char next_symb(Ticker_Message * message);
static char pop_symb(Ticker_Message * message);

//go to the next user string and to the next user string index
//provides a ring queue
static Ticker_Message * next_string(Ticker * ticker)
{
	if(++ticker->cur_string_idx == ticker->strings_count)
		ticker->cur_string_idx = 0;
	return ticker->strings + ticker->cur_string_idx;
}

//go to next ticker:
//	go to delimiter or to user string
//if 'go to user string' - go to the next user string in ticker
static Ticker_Message * next_ticker(Ticker * ticker)
{
	if(ticker->cur_message == ticker->delimiter)
		ticker->cur_message = next_string(ticker);
	else
		ticker->cur_message = ticker->delimiter;
	return ticker->cur_message;
}

static char next_symb(Ticker_Message * message)
{
	if(*message->cur_symb) //if not \0
	{
		return *(++message->cur_symb);
	}
	else
	{
		return *(message->cur_symb = message->string);
	}
}

static char pop_symb(Ticker_Message * message)
{
	char ch = *message->cur_symb;
	next_symb(message);
	return ch;
}

char * ticker_string_pop(Ticker * ticker, char * str_buffer,
		uint16_t length_buffer)
{

	//save current ticker context
	Ticker_Message * cur_message = ticker->cur_message;
	char * cur_symb = ticker->cur_message->cur_symb;
	uint16_t cur_string_idx = ticker->cur_string_idx;

	char * pos_buffer = str_buffer;
	for(uint16_t msg_length;
			length_buffer > 0;
			length_buffer -= msg_length, pos_buffer += msg_length)
	{
		if((msg_length = strlen(ticker->cur_message->cur_symb)) >= length_buffer)
		//current string completely fills str_buffer
		{
			strncpy(pos_buffer, ticker->cur_message->cur_symb, length_buffer);
			break;
		}
		else //current string does not completely fill str_buffer
		{
			strcpy(pos_buffer, ticker->cur_message->cur_symb);

			//current string has been fully read
			//sets the cursor to beginning of the string
			ticker->cur_message->cur_symb = ticker->cur_message->string;
			next_ticker(ticker);
		}
	}
	//restore ticker context
	ticker->cur_message = cur_message;
	ticker->cur_message->cur_symb = cur_symb;
	ticker->cur_string_idx = cur_string_idx;

	//move ticker symbols
	if(!pop_symb(ticker->cur_message)) //if \0
	{
		next_ticker(ticker);
		pop_symb(ticker->cur_message);
	}
	return str_buffer;
}

Ticker_Message ticker_msg_create(char * message)
{
	return (Ticker_Message) {
			.string = message,
			.cur_symb = message
	};
}

Ticker ticker_create(Ticker_Message * messages, uint16_t msg_count,
		Ticker_Message * delimiter, uint16_t display_interval)
{
	return (Ticker) {
			.strings = messages,
			.strings_count = msg_count,
			.cur_string_idx = msg_count - 1,
			.delimiter = delimiter,
			.cur_message = delimiter,
			.display_interval = display_interval
	};
}
