#include <linux/module.h>
#include <linux/jiffies.h>
#include <linux/types.h>

MODULE_LICENSE("GPL");

static u32 j;

static int __init init( void ) {
   /* truncate high 32 bits system clock tick*/
   j = jiffies; 
   printk( KERN_INFO "module: jiffies on start = %X\n", j );   
   return 0;
}

void cleanup( void ) {
   static u32 j1;
   /* truncate high 32 bits system clock tick*/
   j1 = jiffies; 
   printk( KERN_INFO "module: jiffies on finish = %X\n", j1 );   
   j = j1 - j;
   /* j / HZ convert number of tick to seconds */
   printk( KERN_INFO "module: interval of life = %d\n", j / HZ );   
   return;
}

module_init( init );
module_exit( cleanup );
