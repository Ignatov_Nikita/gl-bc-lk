# Author

- Mykyta Ihnatov
- Kiev

# Title

Combination lock

# Description

1. Part of home security system 
2. RPI zero, rotary-encoder HW-040, display 1.8

## References

1. [Video of ...](https://drive.google.com/drive/u/0/folders/1gZBfeqKbTzx9hX1pHqQolECE598R7euZ).
2. [Photo of ...](https://drive.google.com/drive/u/0/folders/1gZBfeqKbTzx9hX1pHqQolECE598R7euZ).

