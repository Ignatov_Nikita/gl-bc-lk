
#ifndef ROTARY_ENCODER_NSY_C
#define ROTARY_ENCODER_NSY_C

#include "rotary-encoder-nsy.h"

static const int8_t direction_rotate[] = { Unknown, CCW,     CW,      Unknown,
					   CW,      Unknown, Unknown, CCW,
					   CCW,     Unknown, Unknown, CW,
					   Unknown, CW,      CCW,     Unknown };

static void (*handler_twist)(int8_t dir);
static void (*handler_btn)(int8_t state);

static int8_t twist_get_dir_rotate(struct _Twist *tw)
{
	uint8_t cur_state;

	cur_state = twist_get_state(tw);

	if (cur_state != tw->prev_state) {
		tw->raw_rt_counter +=
			direction_rotate[(tw->prev_state << 2) | cur_state];

		//if(cur_state == 0x3 || cur_state == 0)
		{
			if (tw->raw_rt_counter >= 1 * tw->turn_sensitive) {
				tw->raw_rt_counter = 0;
				return CW;
			} else if (tw->raw_rt_counter <=
				   -1 * tw->turn_sensitive) {
				tw->raw_rt_counter = 0;
				return CCW;
			}
		}
		tw->prev_state = cur_state;
	}
	return Unknown;
}

int register_rotary_encoder_twist_handler(void *dev,
					  void (*handler)(int8_t direction))
{
	handler_twist = handler;
	return 0;
}
EXPORT_SYMBOL(register_rotary_encoder_twist_handler);

int register_rotary_encoder_btn_handler(void *dev,
					void (*handler)(int8_t state))
{
	handler_btn = handler;
	return 0;
}
EXPORT_SYMBOL(register_rotary_encoder_btn_handler);

void unregister_rotary_encoder_twist_handler(void *dev,
					     void (*handler)(int8_t direction))
{
	if (handler == handler_twist)
		handler_twist = NULL;
}
EXPORT_SYMBOL(unregister_rotary_encoder_twist_handler);

void unregister_rotary_encoder_btn_handler(void *dev,
					   void (*handler)(int8_t state))
{
	if (handler == handler_btn)
		handler_btn = NULL;
}
EXPORT_SYMBOL(unregister_rotary_encoder_btn_handler);

static void twist_handlers_work(struct work_struct *work)
{
	struct _Twist *tw = container_of(work, struct _Twist, work);
	if (handler_twist)
		handler_twist(tw->direction);
}

static irqreturn_t twist_turn_isr(int irq, void *data)
{
	int8_t state;
	struct _Twist *tw = data;
	struct _Rotary_Encoder *encoder =
		container_of(tw, struct _Rotary_Encoder, twist);

	mutex_lock(&tw->access_mutex);

	state = twist_get_dir_rotate(tw);
	switch (state) {
	case CW:
	case CCW:
		tw->direction = state;
		tw->rotation_counter += state;
		input_report_rel(encoder->input, tw->axis, state);
		printk(KERN_DEBUG MODULE_TAG "turn: %d\n",
		       tw->rotation_counter);
		input_sync(encoder->input);
		queue_work(system_wq, &tw->work);
		break;
	}

	mutex_unlock(&tw->access_mutex);

	return IRQ_HANDLED;
}

static void btn_delay_work(struct work_struct *work)
{
	int8_t state;

	struct _Rotary_Encoder *encoder =
		container_of(work, struct _Rotary_Encoder, btn.work.work);
	struct _Button *btn = &encoder->btn;

	mutex_lock(&btn->access_mutex);

	state = btn_get_state(btn);
	switch (state) {
	case BPress:
	case BRelease:
		printk(KERN_DEBUG MODULE_TAG "btn press\n");
		input_report_key(encoder->input, btn->code, state);
		input_sync(encoder->input);
		if (handler_btn)
			handler_btn(state);
		break;

	default:
		break;
	}

	mutex_unlock(&btn->access_mutex);
}

static irqreturn_t btn_isr(int irq, void *data)
{
	struct _Button *btn = data;

	mutex_lock(&btn->access_mutex);

	mod_delayed_work(system_wq, &btn->work,
			 msecs_to_jiffies(btn->software_debounce));

	mutex_unlock(&btn->access_mutex);

	return IRQ_HANDLED;
}

int rotary_encoder_init(struct _Rotary_Encoder *encoder)
{
	int ret;
	struct _Twist *tw = &encoder->twist;
	struct _Button *btn = &encoder->btn;

	mutex_init(&tw->access_mutex);
	mutex_init(&btn->access_mutex);

	ret = gpio_request(tw->pin_a.gpio, tw->pin_a.label);
	if (ret) {
		printk(KERN_ERR MODULE_TAG "failed to request GPIO%d: %d\n",
		       tw->pin_a.gpio, ret);
		//ret = -1;
		goto err0;
	}

	ret = gpio_request(tw->pin_b.gpio, tw->pin_b.label);
	if (ret) {
		printk(KERN_ERR MODULE_TAG "failed to request GPIO%d: %d\n",
		       tw->pin_b.gpio, ret);
		//ret = -1;
		goto err1;
	}
	tw->prev_state = twist_get_state(tw);

	ret = gpio_to_irq(tw->pin_a.gpio);
	if (ret < 0) {
		printk(KERN_ERR MODULE_TAG "failed to get IRQ: %d, GPIO%d\n",
		       ret, tw->pin_a.gpio);
		//ret = -2;
		goto err2;
	}
	tw->irq_a = ret;

	printk(KERN_INFO MODULE_TAG "got IRQ#%d, GPIO%d.\n", ret,
	       tw->pin_a.gpio);

	ret = request_threaded_irq(tw->irq_a, NULL, twist_turn_isr,
				   IRQF_TRIGGER_RISING | IRQF_ONESHOT, DRV_NAME,
				   tw);
	if (ret) {
		printk(KERN_ERR MODULE_TAG
		       "unable to request IRQ %d (gpio#%d)\n",
		       tw->irq_a, tw->pin_a.gpio);
		//ret = -3;
		goto err2;
	}

	ret = gpio_to_irq(tw->pin_b.gpio);
	if (ret < 0) {
		printk(KERN_ERR MODULE_TAG "failed to get IRQ: %d, GPIO%d\n",
		       ret, tw->pin_b.gpio);
		//ret = -2;
		goto err3;
	}
	tw->irq_b = ret;

	printk(KERN_INFO MODULE_TAG "got IRQ#%d, GPIO%d.\n", ret,
	       tw->pin_b.gpio);

	ret = request_threaded_irq(tw->irq_b, NULL, twist_turn_isr,
				   IRQF_TRIGGER_RISING | IRQF_ONESHOT, DRV_NAME,
				   tw);
	if (ret) {
		printk(KERN_ERR MODULE_TAG
		       "unable to request IRQ %d (gpio#%d)\n",
		       tw->irq_b, tw->pin_b.gpio);
		//ret = -3;
		goto err3;
	}

	if (encoder->useBtn) {
		ret = gpio_request(btn->pin_sw.gpio, btn->pin_sw.label);
		if (ret) {
			printk(KERN_ERR MODULE_TAG
			       "failed to request GPIO%d: %d\n",
			       btn->pin_sw.gpio, ret);
			//	ret = -1;
			goto err4;
		}

		ret = gpio_to_irq(btn->pin_sw.gpio);
		if (ret < 0) {
			printk(KERN_ERR MODULE_TAG
			       "failed to get IRQ: %d, GPIO%d\n",
			       ret, btn->pin_sw.gpio);
			//	ret = -2;
			goto err5;
		}
		btn->irq = ret;

		printk(KERN_INFO MODULE_TAG "got IRQ#%d, GPIO%d.\n", ret,
		       btn->pin_sw.gpio);

		INIT_DELAYED_WORK(&btn->work, btn_delay_work);
		INIT_WORK(&tw->work, twist_handlers_work);

		ret = request_threaded_irq(btn->irq, NULL, btn_isr,
					   IRQF_TRIGGER_RISING |
						   IRQF_TRIGGER_FALLING |
						   IRQF_ONESHOT,
					   DRV_NAME, btn);
		if (ret) {
			printk(KERN_ERR MODULE_TAG
			       "unable to request IRQ %d (gpio#%d)\n",
			       btn->irq, btn->pin_sw.gpio);
			//	ret = -3;
			goto err5;
		}
	}

	printk(KERN_INFO MODULE_TAG "device initialized\n");
	return 0;

//err6:
//	free_irq(btn->irq, btn);
err5:
	gpio_free(btn->pin_sw.gpio);
err4:
	free_irq(tw->irq_b, tw);
err3:
	free_irq(tw->irq_a, tw);
err2:
	gpio_free(tw->pin_b.gpio);
err1:
	gpio_free(tw->pin_a.gpio);
err0:
	mutex_destroy(&encoder->twist.access_mutex);
	mutex_destroy(&encoder->btn.access_mutex);
	printk(KERN_ERR MODULE_TAG "device failed to init\n");
	return ret;
}

void rotary_encoder_destroy(struct _Rotary_Encoder *encoder)
{
	if (encoder->useBtn) {
		free_irq(encoder->btn.irq, &encoder->btn);
		gpio_free(encoder->btn.pin_sw.gpio);
	}
	free_irq(encoder->twist.irq_b, &encoder->twist);
	free_irq(encoder->twist.irq_a, &encoder->twist);
	gpio_free(encoder->twist.pin_b.gpio);
	gpio_free(encoder->twist.pin_a.gpio);

	mutex_destroy(&encoder->twist.access_mutex);
	mutex_destroy(&encoder->btn.access_mutex);

	printk(KERN_INFO MODULE_TAG "device destroyed\n");
}

#endif //ROTARY_ENCODER_NSY_H
