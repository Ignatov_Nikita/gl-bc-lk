
#ifndef ROTARY_ENCODER_NSY_H
#define ROTARY_ENCODER_NSY_H

#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/platform_device.h>
#include <linux/workqueue.h>

#define DRV_NAME "rotary-encoder-nsy"
#define MODULE_TAG "rotary-encoder-nsy "

struct _Button {
	struct mutex access_mutex;

	u32 code;
	unsigned int irq;

	struct delayed_work work;
	unsigned int software_debounce;

	struct gpio pin_sw;
	bool pin_active_state;
};

struct _Twist {
	struct mutex access_mutex;

	unsigned int irq_a;
	unsigned int irq_b;

	u32 axis;
	int rotation_counter;
	int8_t direction;

	struct gpio pin_a;
	struct gpio pin_b;
	struct work_struct work;

	uint8_t prev_state;
	int8_t raw_rt_counter;
	uint8_t turn_sensitive;
	uint32_t steps;
};

struct _Rotary_Encoder {
	struct input_dev *input;

	struct _Button btn;

	struct _Twist twist;

	bool useBtn;
};

#define get_clk(twist) (gpio_get_value((twist)->pin_a.gpio))
#define get_dt(twist) (gpio_get_value((twist)->pin_b.gpio))
#define get_sw(btn) (gpio_get_value((btn)->pin_sw.gpio))

#define twist_get_state(twist) ((get_clk(twist) << 1) | (get_dt(twist)))

enum Button_state { BUnknown = -1, BRelease, BPress };

#define btn_get_state(btn)                                                     \
	(get_sw(btn) == (btn)->pin_active_state ? BPress : BRelease)

static inline int8_t __btn_get_state(struct _Button *btn)
{
	if (get_sw(btn) == btn->pin_active_state) {
		return BPress;
	} else {
		return BRelease;
	}
}

enum Twist_dir_rotate { CCW = -1, Unknown, CW };

int rotary_encoder_init(struct _Rotary_Encoder *encoder);
void rotary_encoder_destroy(struct _Rotary_Encoder *encoder);

int register_rotary_encoder_twist_handler(void *dev,
					  void (*handler)(int8_t direction));
int register_rotary_encoder_btn_handler(void *dev,
					void (*handler)(int8_t state));

void unregister_rotary_encoder_twist_handler(void *dev,
					     void (*handler)(int8_t direction));
void unregister_rotary_encoder_btn_handler(void *dev,
					   void (*handler)(int8_t state));

#endif //ROTARY_ENCODER_NSY_H
