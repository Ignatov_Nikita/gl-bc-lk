
#ifndef _RW_C_
#define _RW_C_

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/fs.h>
#include "rotary-encoder-nsy.h"

MODULE_AUTHOR("Mykyta.Ihnatov <nikitasinys@gmail.com>");
MODULE_DESCRIPTION("Rotary-encoder driver");
MODULE_INFO(intree, "Y");
MODULE_LICENSE("GPL");

#define SYS_DIRECTORY "Rotary-encoder-nsy"
//*
#define EN1_CLK 13
#define EN1_DT 19
#define EN1_SW 26
//*/
static struct _Rotary_Encoder *_encoder;

static inline int twist_get_rotation_counter(struct _Rotary_Encoder *encoder)
{
	int counter;
	mutex_lock(&encoder->twist.access_mutex);
	counter = encoder->twist.rotation_counter;
	mutex_unlock(&encoder->twist.access_mutex);
	return counter;
}

static inline void twist_set_rotation_counter(int _counter,
					      struct _Rotary_Encoder *encoder)
{
	mutex_lock(&encoder->twist.access_mutex);
	encoder->twist.rotation_counter = _counter;
	mutex_unlock(&encoder->twist.access_mutex);
}

static inline uint8_t twist_get_turn_sensitive(struct _Rotary_Encoder *encoder)
{
	uint8_t sensitive;
	mutex_lock(&encoder->twist.access_mutex);
	sensitive = encoder->twist.turn_sensitive;
	mutex_unlock(&encoder->twist.access_mutex);
	return sensitive;
}

static inline void twist_set_turn_sensitive(uint8_t sensitive,
					    struct _Rotary_Encoder *encoder)
{
	mutex_lock(&encoder->twist.access_mutex);
	encoder->twist.turn_sensitive = sensitive;
	mutex_unlock(&encoder->twist.access_mutex);
}

static inline unsigned int
btn_get_debounce_time(struct _Rotary_Encoder *encoder)
{
	unsigned int msec;
	mutex_lock(&encoder->btn.access_mutex);
	msec = encoder->btn.software_debounce;
	mutex_unlock(&encoder->btn.access_mutex);
	return msec;
}

static inline void btn_set_debounce_time(unsigned int msec,
					 struct _Rotary_Encoder *encoder)
{
	mutex_lock(&encoder->btn.access_mutex);
	encoder->btn.software_debounce = msec;
	mutex_unlock(&encoder->btn.access_mutex);
}

static ssize_t counter_show(struct class *class, struct class_attribute *attr,
			    char *buffer)
{
	ssize_t length = 0;

	printk(KERN_DEBUG "in counter_show length: %zu\n\n", strlen(buffer));

	if (_encoder != NULL)
		length = sprintf(buffer, "%d\n",
				 twist_get_rotation_counter(_encoder));
	else
		printk(KERN_ERR MODULE_TAG "failed, _encoder == NULL\n");

	printk(KERN_DEBUG "in counter_show\tstr_usr: %s\n", buffer);

	printk(KERN_NOTICE MODULE_TAG "counter_show %zx chars\n", length);

	return length;
}

static ssize_t counter_store(struct class *class, struct class_attribute *attr,
			     const char *buffer, size_t length)
{
	int res;
	int counter;

	printk(KERN_DEBUG "in counter_store\n");

	res = kstrtoint(buffer, 10, &counter);
	if (res < 0) {
		printk(KERN_ERR MODULE_TAG "failed to parse counter %s \n",
		       buffer);
		return 0;
	}

	if (_encoder != NULL)
		twist_set_rotation_counter(counter, _encoder);
	else
		printk(KERN_ERR MODULE_TAG "failed, _encoder == NULL\n");

	printk(KERN_NOTICE MODULE_TAG "written %zx chars\n", length);

	return length;
}

static ssize_t turn_sensitive_show(struct class *class,
				   struct class_attribute *attr, char *buffer)
{
	ssize_t length = 0;

	printk(KERN_DEBUG "in turn_sensitive_show length: %zu\n\n",
	       strlen(buffer));

	if (_encoder != NULL)
		length = sprintf(buffer, "%d\n",
				 twist_get_turn_sensitive(_encoder));
	else
		printk(KERN_ERR MODULE_TAG "failed, _encoder == NULL\n");

	printk(KERN_DEBUG "in turn_sensitive_show\tstr_usr: %s\n", buffer);

	printk(KERN_NOTICE MODULE_TAG "turn_sensitive_show %zx chars\n",
	       length);

	return length;
}

static ssize_t turn_sensitive_store(struct class *class,
				    struct class_attribute *attr,
				    const char *buffer, size_t length)
{
	int res;
	uint8_t sensitive;

	printk(KERN_DEBUG "in turn_sensitive_store\n");

	res = kstrtou8(buffer, 10, &sensitive);
	if (res < 0) {
		printk(KERN_ERR MODULE_TAG "failed to parse sensitive %s \n",
		       buffer);
		return 0;
	}

	if (_encoder != NULL)
		twist_set_turn_sensitive(sensitive, _encoder);
	else
		printk(KERN_ERR MODULE_TAG "failed, _encoder == NULL\n");

	printk(KERN_NOTICE MODULE_TAG "written %zx chars\n", length);

	return length;
}

static ssize_t debounce_time_show(struct class *class,
				  struct class_attribute *attr, char *buffer)
{
	ssize_t length = 0;

	printk(KERN_DEBUG "in debounce_time_show length: %zu\n\n",
	       strlen(buffer));

	if (_encoder != NULL)
		length = sprintf(buffer, "%d\n",
				 btn_get_debounce_time(_encoder));
	else
		printk(KERN_ERR MODULE_TAG "failed, _encoder == NULL\n");

	printk(KERN_DEBUG "in debounce_time_show\tstr_usr: %s\n", buffer);

	printk(KERN_NOTICE MODULE_TAG "read debounce_time_show %zx chars\n",
	       length);

	return length;
}

static ssize_t debounce_time_store(struct class *class,
				   struct class_attribute *attr,
				   const char *buffer, size_t length)
{
	int res;
	unsigned int time;

	printk(KERN_DEBUG "in debounce_time_store\n");

	res = kstrtouint(buffer, 10, &time);
	if (res < 0) {
		printk(KERN_ERR MODULE_TAG "failed to parse counter %s \n",
		       buffer);
		return 0;
	}

	if (_encoder != NULL)
		btn_set_debounce_time(time, _encoder);
	else
		printk(KERN_ERR MODULE_TAG "failed, _encoder == NULL\n");

	printk(KERN_NOTICE MODULE_TAG "written %zx chars\n", length);

	return length;
}

CLASS_ATTR_RW(debounce_time);
CLASS_ATTR_RW(counter);
CLASS_ATTR_RW(turn_sensitive);
//CLASS_ATTR_RW( direction );
static struct class *encoder_class;

static int create_sys_info(void)
{
	int err;

	encoder_class = class_create(THIS_MODULE, SYS_DIRECTORY);
	if (IS_ERR(encoder_class))
		printk("bad class create\n");

	err = class_create_file(encoder_class, &class_attr_counter);
	if (err)
		return -EFAULT;
	/*
	err = class_create_file( encoder_class, &class_attr_direction );
	if (err)
		return -EFAULT;
//*/
	err = class_create_file(encoder_class, &class_attr_debounce_time);
	if (err)
		return -EFAULT;
	err = class_create_file(encoder_class, &class_attr_turn_sensitive);
	if (err)
		return -EFAULT;

	return 0;
}

static void cleanup_sys_info(void)
{
	class_remove_file(encoder_class, &class_attr_counter);
	//	class_remove_file( encoder_class, &class_attr_direction );
	class_remove_file(encoder_class, &class_attr_debounce_time);
	class_remove_file(encoder_class, &class_attr_turn_sensitive);
	class_destroy(encoder_class);
}

int rotary_encoder_probe(struct platform_device *pdev)
{
	int res;
	struct device *dev = &pdev->dev;
	struct input_dev *input;
	struct device_node *node = dev->of_node;
	struct _Rotary_Encoder *encoder;

	printk(KERN_NOTICE MODULE_TAG "start probe\n");

	encoder = devm_kzalloc(dev, sizeof(struct _Rotary_Encoder), GFP_KERNEL);
	if (!encoder)
		return -ENOMEM;

	of_property_read_u32(node, "axis", &encoder->twist.axis);

	of_property_read_u32(node, "pin_a", &encoder->twist.pin_a.gpio);
	of_property_read_u32(node, "pin_b", &encoder->twist.pin_b.gpio);
	of_property_read_u32(node, "pin_sw", &encoder->btn.pin_sw.gpio);

	encoder->twist.pin_a.flags = GPIOF_IN;
	encoder->twist.pin_a.label = "Encoder CLK (A)";
	encoder->twist.pin_b.flags = GPIOF_IN;
	encoder->twist.pin_b.label = "Encoder DT (B)";

	of_property_read_u32(node, "turn_sensitive", &res);
	if (res == 0 || res > 100)
		encoder->twist.turn_sensitive = 6;
	else
		encoder->twist.turn_sensitive = res;

	encoder->useBtn = of_property_read_bool(node, "use_btn");
	if (encoder->useBtn) {
		of_property_read_u32(node, "btn_code", &encoder->btn.code);

		encoder->btn.pin_sw.flags = GPIOF_IN;
		encoder->btn.pin_sw.label = "Encoder SW";
		encoder->btn.pin_active_state = 0;

		of_property_read_u32(node, "btn_debounce",
				     &encoder->btn.software_debounce);
	}
	//	encoder->btn.software_debounce = 50;
	//	encoder->twist.turn_sensitive = 6;
	//	encoder->useBtn = true;

	input = devm_input_allocate_device(dev);
	if (!input)
		return -ENOMEM;

	encoder->input = input;

	input->name = pdev->name;
	input->id.bustype = BUS_HOST;
	input->dev.parent = dev;

	input_set_capability(input, EV_REL, encoder->twist.axis);
	//input_set_keycode(input, EV_KEY, encoder->twist.axis);
	set_bit(EV_KEY, input->evbit);
	set_bit(encoder->btn.code, input->keybit);

	res = rotary_encoder_init(encoder);
	if (res) {
		dev_err(dev, "failed to init device\n");
		return res;
	}

	res = input_register_device(input);
	if (res) {
		dev_err(dev, "failed to register input device\n");
		return res;
	}

	device_init_wakeup(dev, of_property_read_bool(node, "wakeup-source"));

	platform_set_drvdata(pdev, encoder);
	_encoder = encoder;

	printk(KERN_NOTICE MODULE_TAG "successfully probe\n");
	return 0;
}

static int rotary_encoder_remove(struct platform_device *pdev)
{
	struct _Rotary_Encoder *encoder = platform_get_drvdata(pdev);
	rotary_encoder_destroy(encoder);
	input_unregister_device(encoder->input);
	encoder->input = 0;
	//	platform_device_unregister(pdev);

	printk(KERN_NOTICE MODULE_TAG "successfully remove\n");
	return 0;
}

static int __maybe_unused rotary_encoder_suspend(struct device *dev)
{
	struct _Rotary_Encoder *encoder = dev_get_drvdata(dev);

	printk(KERN_DEBUG "rotary_encoder_suspend start\n");
	if (device_may_wakeup(dev)) {
		printk(KERN_DEBUG "suspend\n");
		enable_irq_wake(encoder->twist.irq_a);
		enable_irq_wake(encoder->twist.irq_b);
		enable_irq_wake(encoder->btn.irq);
	}

	return 0;
}

static int __maybe_unused rotary_encoder_resume(struct device *dev)
{
	struct _Rotary_Encoder *encoder = dev_get_drvdata(dev);

	printk(KERN_DEBUG "rotary_encoder_resume start\n");
	if (device_may_wakeup(dev)) {
		printk(KERN_DEBUG "resume\n");
		disable_irq_wake(encoder->twist.irq_a);
		disable_irq_wake(encoder->twist.irq_b);
		disable_irq_wake(encoder->btn.irq);
	}
	return 0;
}

static SIMPLE_DEV_PM_OPS(rotary_encoder_pm_ops, rotary_encoder_suspend,
			 rotary_encoder_resume);

static const struct of_device_id rotary_encoder_of_match[] = {
	{
		.compatible = "rotary-encoder-nsy",
	},
	{},
};
MODULE_DEVICE_TABLE(of, rotary_encoder_of_match);

static struct platform_driver rotary_encoder_nsy_driver =
	{ .probe = rotary_encoder_probe,
	  .remove = rotary_encoder_remove,
	  .driver = {
		  .name = DRV_NAME,
		  .pm = &rotary_encoder_pm_ops,
		  .of_match_table = of_match_ptr(rotary_encoder_of_match),
	  } };

static int __init rotary_encoder_rw_init(void)
{
	int err;

	printk(KERN_NOTICE MODULE_TAG "init\n");

	err = platform_driver_register(&rotary_encoder_nsy_driver);
	if (err < 0)
		goto err0;

	err = create_sys_info();
	if (err)
		goto err1;

	printk(KERN_NOTICE MODULE_TAG "loaded\n");
	return 0;

//err2:
// cleanup_sys_info();
err1:
	platform_driver_unregister(&rotary_encoder_nsy_driver);
err0:
	printk(KERN_ERR MODULE_TAG "failed to load\n");
	return err;
}

static void __exit rotary_encoder_rw_exit(void)
{
	cleanup_sys_info();

	platform_driver_unregister(&rotary_encoder_nsy_driver);
	printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(rotary_encoder_rw_init);
module_exit(rotary_encoder_rw_exit);

#endif
