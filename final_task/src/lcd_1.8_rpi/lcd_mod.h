
#ifndef _LCD_MOD_H
#define _LCD_MOD_H

#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include "st7735.h"
#include "fonts.c"

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_AUTHOR("Mykyta.Ihnatov <nikitasinys@gmail.com>");
MODULE_DESCRIPTION("LCD 1.8");

inline void lcd_1_8_update_screen(void);

void lcd_1_8_draw_pixel(u16 x, u16 y, u16 color);

void lcd_1_8_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color);

void lcd_1_8_fill_screen(u16 color);

void lcd_1_8_put_char(u16 x, u16 y, char ch, FontDef font, u16 color,
		      u16 bgcolor);

void lcd_1_8_put_str(u16 x, u16 y, const char *str, FontDef font, u16 color,
		     u16 bgcolor);

#endif
