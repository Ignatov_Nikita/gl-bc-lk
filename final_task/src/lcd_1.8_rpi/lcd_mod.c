
#ifndef _LCD_MOD_C
#define _LCD_MOD_C

#include "lcd_mod.h"

static u16 frame_buffer[LCD_WIDTH * LCD_HEIGHT];
static struct spi_device *lcd_spi_device;

static void lcd_reset(void)
{
	gpio_set_value(LCD_PIN_RESET, 0);
	mdelay(5);
	gpio_set_value(LCD_PIN_RESET, 1);
}

static void lcd_write_command(u8 cmd)
{
	gpio_set_value(LCD_PIN_DC, 0);
	spi_write(lcd_spi_device, &cmd, sizeof(cmd));
}

static void lcd_write_data(u8 *buff, size_t buff_size)
{
	size_t i = 0;
	
	gpio_set_value(LCD_PIN_DC, 1);
	while (buff_size > DATA_SIZE) {
		spi_write(lcd_spi_device, buff + i, DATA_SIZE);
		i += DATA_SIZE;
		buff_size -= DATA_SIZE;
	}
	spi_write(lcd_spi_device, buff + i, buff_size);
}

static void lcd_set_address_window(u16 x0, u16 y0, u16 x1, u16 y1)
{

	lcd_write_command(LCD_CASET);
	{
		uint8_t data[] = { (x0 >> 8) & 0xFF, x0 & 0xFF,
				 (x1 >> 8) & 0xFF, x1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RASET);
	{
		uint8_t data[] = { (y0 >> 8) & 0xFF, y0 & 0xFF,
				(y1 >> 8) & 0xFF, y1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RAMWR);
}

inline void lcd_1_8_update_screen(void)
{
	lcd_write_data((u8*)frame_buffer, sizeof(u16) * LCD_WIDTH * LCD_HEIGHT);
}
EXPORT_SYMBOL(lcd_1_8_update_screen);

void lcd_1_8_draw_pixel(u16 x, u16 y, u16 color)
{
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}
        
	frame_buffer[x + LCD_WIDTH * y] = (color >> 8) | (color << 8);
	lcd_1_8_update_screen();
}
EXPORT_SYMBOL(lcd_1_8_draw_pixel);

void lcd_1_8_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color)
{
	u16 i;
	u16 j;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}

	if ((x + w - 1) > LCD_WIDTH) {
		w = LCD_WIDTH - x;
	}

	if ((y + h - 1) > LCD_HEIGHT) {
		h = LCD_HEIGHT - y;
	}

	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			frame_buffer[(x + LCD_WIDTH * y) + (i + LCD_WIDTH * j)] = (color >> 8) | (color << 8);
		}
	}
	lcd_1_8_update_screen();
}
EXPORT_SYMBOL(lcd_1_8_fill_rectangle);

void lcd_1_8_fill_screen(u16 color)
{
	lcd_1_8_fill_rectangle(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, color);
}
EXPORT_SYMBOL(lcd_1_8_fill_screen);

void lcd_1_8_put_char(u16 x, u16 y, char ch, FontDef font, u16 color, u16 bgcolor) 
{
	u32 i, b, j;

	for (i = 0; i < font.height; i++) {
		b = font.data[(ch - 32) * font.height + i];
		for (j = 0; j < font.width; j++) {
			if ((b << j) & 0x8000)  {
				frame_buffer[(x + LCD_WIDTH * y) + (j + LCD_WIDTH * i)] = 
					(color >> 8) | (color << 8);
			} 
			else {
				frame_buffer[(x + LCD_WIDTH * y) + (j + LCD_WIDTH * i)] = 
					(bgcolor >> 8) | (bgcolor << 8);
			}
		}
	}
}
EXPORT_SYMBOL(lcd_1_8_put_char);

void lcd_1_8_put_str(u16 x, u16 y, const char* str, FontDef font, u16 color, u16 bgcolor) 
{
	while (*str) {
		if (x + font.width >= LCD_WIDTH) {
			x = 0;
			y += font.height;
			if (y + font.height >= LCD_HEIGHT) {
				break;
			}

			if (*str == ' ') {
				// skip spaces in the beginning of the new line
				str++;
				continue;
			}
		}
		lcd_1_8_put_char(x, y, *str, font, color, bgcolor);
		x += font.width;
		str++;
	}
}
EXPORT_SYMBOL(lcd_1_8_put_str);

static void lcd_init_st7735(void)
{
	lcd_write_command(ST7735_SWRESET);
	mdelay(150);

	lcd_write_command(ST7735_SLPOUT);
	mdelay(250);

	lcd_write_command(ST7735_FRMCTR1);
	{
		u8 data[] = { 0x01, 0x2C, 0x2D };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_FRMCTR2);
	{
		u8 data[] = { 0x01, 0x2C, 0x2D };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_FRMCTR3);
	{
		u8 data[] = { 0x01, 0x2C, 0x2D, 0x01, 0x2C, 0x2D };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_INVCTR);
	{
		u8 data[] = { 0x07 };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_PWCTR1);
	{
		u8 data[] = { 0xA2, 0x02, 0x84 };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_PWCTR2);
	{
		u8 data[] = { 0xC5 };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_PWCTR3);
	{
		u8 data[] = { 0x0A, 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_PWCTR4);
	{
		u8 data[] = { 0x8A, 0x2A };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_PWCTR5);
	{
		u8 data[] = { 0x8A, 0xEE };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_VMCTR1);
	{
		u8 data[] = { 0x0E };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_INVOFF);
	
	lcd_write_command(ST7735_MADCTL);
	{
		u8 data[] = { LCD_ROTATION };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_COLMOD);
	{
		u8 data[] = { 0x05 };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_CASET);
	{
		u8 data[] = { 0x00, 0x00, 0x00, 0x9F };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_RASET);
	{
		u8 data[] = { 0x00, 0x00, 0x00, 0x7F };
		lcd_write_data(data, sizeof(data));
	}
	
	lcd_write_command(ST7735_GMCTRP1);
	{
		u8 data[] = { 0x02, 0x1c, 0x07, 0x12, 0x37, 0x32, 0x29, 0x2d,
				0x29, 0x25, 0x2B, 0x39, 0x00, 0x01, 0x03, 0x10 };
		lcd_write_data(data, sizeof(data));
	}
	
	lcd_write_command(ST7735_GMCTRN1);
	{
		u8 data[] = { 0x03, 0x1d, 0x07, 0x06, 0x2E, 0x2C, 0x29, 0x2D,
      				0x2E, 0x2E, 0x37, 0x3F, 0x00, 0x00, 0x02, 0x10 };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(ST7735_NORON);
	mdelay(10);
    
	lcd_write_command(ST7735_DISPON);
	mdelay(10);
}

static void __exit mod_exit(void)
{

	gpio_free(LCD_PIN_DC);
	gpio_free(LCD_PIN_RESET);

	if (lcd_spi_device) {
		spi_unregister_device(lcd_spi_device);
	}
	pr_info("LCD: spi device unregistered\n");
	pr_info("LCD: module exited\n");
}

static int __init mod_init(void)
{
	int ret;
	struct spi_master *master;

	struct spi_board_info lcd_info = {
		.modalias = "LCD",
		.max_speed_hz = 25e6,
		.bus_num = 0,
		.chip_select = 0,
		.mode = SPI_MODE_0,
	};

	master = spi_busnum_to_master(lcd_info.bus_num);
	if (!master) {
		printk("MASTER not found.\n");
		ret = -ENODEV;
		goto out;
	}

	lcd_spi_device = spi_new_device(master, &lcd_info);
	if (!lcd_spi_device) {
		printk("FAILED to create slave.\n");
		ret = -ENODEV;
		goto out;
	}

	lcd_spi_device->bits_per_word = 8,

	ret = spi_setup(lcd_spi_device);
	if (ret) {
		printk("FAILED to setup slave.\n");
		spi_unregister_device(lcd_spi_device);
		ret = -ENODEV;
		goto out;
	}

	pr_info("LCD: spi device setup completed\n");

	gpio_request(LCD_PIN_RESET, "LCD_PIN_RESET");
	gpio_direction_output(LCD_PIN_RESET, 0);
	gpio_request(LCD_PIN_DC, "LCD_PIN_DC");
	gpio_direction_output(LCD_PIN_DC, 0);
	lcd_reset();

	lcd_init_st7735();

	memset(frame_buffer, COLOR_BLACK, sizeof(frame_buffer));
	lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
	
	lcd_1_8_put_str(0, 40, "LCD 1.8", Font_16x26, COLOR_BLUE, COLOR_BLACK);
	lcd_1_8_update_screen();

 	pr_info("LCD: module loaded\n");

	return 0;

out:
	return ret;
}

module_init(mod_init);
module_exit(mod_exit);

#endif

