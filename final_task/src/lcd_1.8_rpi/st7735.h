#ifndef __ST7735_H__
#define __ST7735_H__

#define LCD_PIN_CS 8//7
#define LCD_PIN_RESET 25
#define LCD_PIN_DC 24
#define DATA_SIZE	90
#define ST7735_MADCTL_MY  0x80
#define ST7735_MADCTL_MX  0x40
#define ST7735_MADCTL_MV  0x20
#define ST7735_MADCTL_ML  0x10
#define ST7735_MADCTL_RGB 0x00
#define ST7735_MADCTL_BGR 0x08
#define ST7735_MADCTL_MH  0x04

// default orientation
#define LCD_WIDTH  160
#define LCD_HEIGHT 128
#define LCD_ROTATION ( ST7735_MADCTL_MV | ST7735_MADCTL_MX | ST7735_MADCTL_RGB)

// Commands
#define ST7735_SWRESET 0x01 // SoftWare Reset
#define ST7735_SLPOUT 0x11 // Sleep Out
#define ST7735_FRMCTR1 0xB1 // Norman Mode (Full colors)
#define ST7735_FRMCTR2 0xB2 // In idle Mode (8 colors)
#define ST7735_FRMCTR3 0xB3 // In partial Mode + Full Colors
#define ST7735_INVCTR 0xB4 // Display inversion control
#define ST7735_PWCTR1 0xC0 // Power control setting
#define ST7735_PWCTR2 0xC1 // Power control setting
#define ST7735_PWCTR3 0xC2 // In normal mode (Full colors)
#define ST7735_PWCTR4 0xC3 // In idle mode (8 colors)
#define ST7735_PWCTR5 0xC4 // In partial mode + Full colors
#define ST7735_VMCTR1 0xC5 // VCOM control 1
#define ST7735_INVOFF 0x20 // Display inversion off
#define ST7735_MADCTL 0x36 // Memory data access control
#define ST7735_COLMOD 0x3A // Interface pixel format
#define ST7735_CASET 0x2A
#define ST7735_RASET 0x2B
#define ST7735_GMCTRP1 0xE0
#define ST7735_GMCTRN1 0xE1
#define ST7735_NORON 0x13
#define ST7735_DISPON 0x29
#define ST7735_DISPOFF 0x28
#define ST7735_RAMWR 0x2C

#define LCD_CASET 0x2A
#define LCD_RASET 0x2B
#define LCD_RAMWR 0x2C

/****************************/

// Color definitions
#define COLOR_BLACK   0x0000
#define COLOR_BLUE    0x001F
#define COLOR_RED     0xF800
#define COLOR_GREEN   0x07E0
#define COLOR_CYAN    0x07FF
#define COLOR_MAGENTA 0xF81F
#define COLOR_YELLOW  0xFFE0
#define COLOR_WHITE   0xFFFF
#define COLOR_COLOR565(r, g, b) (((r & 0xF8) << 8) | ((g & 0xFC) << 3) | ((b & 0xF8) >> 3))


#endif // __ST7735_H__
