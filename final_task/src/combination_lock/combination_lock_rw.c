
#ifndef _COMBINATION_LOCK_RW_C_
#define _COMBINATION_LOCK_RW_C_

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/fs.h>
#include "combination_lock.h"
#include "combination_lock_display.h"

MODULE_AUTHOR("Mykyta.Ihnatov <nikitasinys@gmail.com>");
MODULE_DESCRIPTION("Combination lock module");
MODULE_INFO(intree, "Y");
MODULE_LICENSE("GPL");

#define SYS_DIRECTORY "Combination_lock"

static struct Combination_lock comb_lock;
static struct Password password;
static struct Dictionary dictionary;
static int8_t btn_state;

extern int
register_rotary_encoder_twist_handler(void *dev,
				      void (*handler)(int8_t direction));
extern int register_rotary_encoder_btn_handler(void *dev,
					       void (*handler)(int8_t state));
extern int
unregister_rotary_encoder_twist_handler(void *dev,
					void (*handler)(int8_t direction));
extern int unregister_rotary_encoder_btn_handler(void *dev,
						 void (*handler)(int8_t state));

static void twist_turn(int8_t direction)
{
	switch (direction) {
	case 1:
		if (btn_state) {
			comb_lock_change_symb_next(&comb_lock);
		} else {
			comb_lock_next_symb(&comb_lock);
		}
		break;
	case -1:
		if (btn_state) {
			comb_lock_change_symb_prev(&comb_lock);
		} else {
			comb_lock_prev_symb(&comb_lock);
		}
		break;
	}
	if (comb_lock_try_open(&comb_lock)) {
		comb_lock_print_lock_is_open(&comb_lock);
	}
	comb_lock_print(&comb_lock);
}

static void btn_action(int8_t state)
{
	btn_state = state;
}

static int __init combination_lock_rw_init(void)
{
	int err;

	printk(KERN_NOTICE MODULE_TAG "init\n");

	err = combination_lock_init(&comb_lock, 5);
	if (err)
		goto err0;
	err = dictionary_init(&dictionary,
			      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
	if (err) {
		printk(KERN_ERR MODULE_TAG "dictionary_init: %d\n", err);
		goto err1;
	}
	err = comb_lock_set_dictionary(&comb_lock, &dictionary);
	if (err) {
		printk(KERN_ERR MODULE_TAG "comb_lock_set_dictionary: %d\n",
		       err);
		goto err2;
	}
	err = comb_lock_set_input_str(&comb_lock, 0);
	if (err) {
		printk(KERN_ERR MODULE_TAG "comb_lock_set_input_str: %d\n",
		       err);
		goto err2;
	}
	err = comb_lock_set_mask_str(&comb_lock, "*****");
	if (err) {
		printk(KERN_ERR MODULE_TAG "comb_lock_set_output_str: %d\n",
		       err);
		goto err2;
	}
	err = password_init(&password, "AABCA");
	if (err) {
		printk(KERN_ERR MODULE_TAG "password_init: %d\n", err);
		goto err2;
	}
	err = comb_lock_add_password(&comb_lock, &password);
	if (err) {
		printk(KERN_ERR MODULE_TAG "comb_lock_add_password: %d\n", err);
		goto err3;
	}
	err = comb_lock_check_object_is_correct(&comb_lock);
	if (err) {
		printk(KERN_ERR MODULE_TAG
		       "comb_lock_check_object_is_correct: %d\n",
		       err);
		goto err3;
	}

	err = register_rotary_encoder_btn_handler(NULL, btn_action);
	if (err) {
		printk(KERN_ERR MODULE_TAG
		       "register_rotary_encoder_btn_handler: %d\n",
		       err);
		goto err3;
	}
	err = register_rotary_encoder_twist_handler(NULL, twist_turn);
	if (err) {
		printk(KERN_ERR MODULE_TAG
		       "register_rotary_encoder_btn_handler: %d\n",
		       err);
		goto err4;
	}

	printk(KERN_NOTICE MODULE_TAG "loaded\n");

	printk(KERN_DEBUG MODULE_TAG "input: %s\n", comb_lock.input_str);
	printk(KERN_DEBUG MODULE_TAG "output: %s\n", comb_lock.output_str);
	printk(KERN_DEBUG MODULE_TAG "dictionary: %s\n",
	       comb_lock.dictionary->str);

	comb_lock_print_start_screen(&comb_lock);
	comb_lock_print(&comb_lock);
	return 0;

//err5:
//	unregister_rotary_encoder_twist_handler(NULL, twist_turn);
err4:
	unregister_rotary_encoder_btn_handler(NULL, btn_action);
err3:
	password_cleanup(&password);
err2:
	dictionary_cleanup(&dictionary);
err1:
	combination_lock_destroy(&comb_lock);
err0:
	printk(KERN_ERR MODULE_TAG "failed to load\n");
	return err;
}

static void __exit combination_lock_rw_exit(void)
{
	unregister_rotary_encoder_twist_handler(NULL, twist_turn);
	unregister_rotary_encoder_btn_handler(NULL, btn_action);
	combination_lock_destroy(&comb_lock);
	password_cleanup(&password);
	dictionary_cleanup(&dictionary);
	printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(combination_lock_rw_init);
module_exit(combination_lock_rw_exit);

#endif
