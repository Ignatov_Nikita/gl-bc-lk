
#ifndef _COMBINATION_LOCK_H_
#define _COMBINATION_LOCK_H_

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/slab.h>

#define MODULE_TAG "combination_lock "

struct Password {
	char *str;
	uint16_t buff_size;
	uint level;
};

struct Dictionary {
	char *str;
	uint8_t buff_size;
	uint8_t str_length;
};

struct Combination_lock {
	uint16_t str_length;
	uint16_t str_idx;
	uint8_t *dictionary_symb_indexes;
	char *input_str;
	char *output_str;
	char *mask_str;
	char last_symb;
	struct Dictionary *dictionary;
	struct Password *password;
};

int comb_lock_switch_output(struct Combination_lock *comb_lock, bool mask);
int comb_lock_set_mask_str(struct Combination_lock *comb_lock, char *str);
int comb_lock_set_input_str(struct Combination_lock *comb_lock,
			    uint8_t idx_el_in_dict);

int password_init(struct Password *password, char *str);
void password_cleanup(struct Password *password);
int password_set_str(struct Password *password, char *str);
int comb_lock_add_password(struct Combination_lock *comb_lock,
			   struct Password *password);

int dictionary_init(struct Dictionary *dictionary, char *str);
void dictionary_cleanup(struct Dictionary *dictionary);
int dictionary_set_str(struct Dictionary *dictionary, char *str);
int comb_lock_set_dictionary(struct Combination_lock *comb_lock,
			     struct Dictionary *dictionary);

int combination_lock_init(struct Combination_lock *comb_lock,
			  uint16_t str_length);
void combination_lock_destroy(struct Combination_lock *comb_lock);

int comb_lock_check_object_is_correct(struct Combination_lock *comb_lock);

void comb_lock_change_symb_next(struct Combination_lock *comb_lock);
void comb_lock_next_symb(struct Combination_lock *comb_lock);
void comb_lock_change_symb_prev(struct Combination_lock *comb_lock);
void comb_lock_prev_symb(struct Combination_lock *comb_lock);

#define comb_lock_symb_get_idx_in_dictionary(combination_lock)                 \
	(combination_lock)->dictionary_symb_indexes[(combination_lock)->str_idx]

inline char
comb_lock_symb_get_next_symb_in_dictionary(struct Combination_lock *comb_lock);
inline uint8_t
comb_lock_symb_get_next_idx_in_dictionary(struct Combination_lock *comb_lock);
inline char
comb_lock_symb_get_prev_symb_in_dictionary(struct Combination_lock *comb_lock);
inline uint8_t
comb_lock_symb_get_prev_idx_in_dictionary(struct Combination_lock *comb_lock);

bool comb_lock_try_open(struct Combination_lock *comb_lock);
#endif
