
#ifndef _COMBINATION_LOCK_DISPLAY_H_
#define _COMBINATION_LOCK_DISPLAY_H_

#include "combination_lock.h"

void comb_lock_print_str(struct Combination_lock *comb_lock);
void comb_lock_print_symb(struct Combination_lock *comb_lock);
void comb_lock_print_lock_is_open(struct Combination_lock *comb_lock);
void comb_lock_print_lock_is_close(struct Combination_lock *comb_lock);
void comb_lock_print(struct Combination_lock *comb_lock);
void comb_lock_print_start_screen(struct Combination_lock *comb_lock);

#endif
