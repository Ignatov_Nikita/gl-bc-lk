
#ifndef _COMBINATION_LOCK_C_
#define _COMBINATION_LOCK_C_

#include "combination_lock.h"

static inline void
comb_lock_set_symb_from_input(struct Combination_lock *comb_lock)
{
	comb_lock->output_str[comb_lock->str_idx] =
		comb_lock->input_str[comb_lock->str_idx];
}

int comb_lock_switch_output(struct Combination_lock *comb_lock, bool mask)
{
	if (mask) {
		if (strlen(comb_lock->mask_str) != comb_lock->str_length)
			return -EINVAL;
		comb_lock->output_str = comb_lock->mask_str;
	} else {
		comb_lock->last_symb = comb_lock->input_str[comb_lock->str_idx];
		comb_lock->output_str = comb_lock->input_str;
	}
	return 0;
}

int comb_lock_set_input_str(struct Combination_lock *comb_lock,
			    uint8_t idx_el_in_dict)
{
	uint16_t idx;
	if (idx_el_in_dict >= comb_lock->dictionary->str_length) {
		return -EINVAL;
	}

	for (idx = 0; idx < comb_lock->str_length; idx++) {
		comb_lock->input_str[idx] =
			comb_lock->dictionary->str[idx_el_in_dict];
		comb_lock->dictionary_symb_indexes[idx] = idx_el_in_dict;
	}
	comb_lock->input_str[idx] = '\0';
	if (comb_lock->output_str != comb_lock->mask_str)
		comb_lock->last_symb = comb_lock->input_str[comb_lock->str_idx];
	return 0;
}

int comb_lock_set_mask_str(struct Combination_lock *comb_lock, char *str)
{
	if (str == NULL || comb_lock->mask_str == NULL) {
		return -EFAULT;
	}
	if (strlen(str) != comb_lock->str_length) {
		return -EINVAL;
	}
	strcpy(comb_lock->mask_str, str);
	comb_lock->output_str = comb_lock->mask_str;
	comb_lock->last_symb = comb_lock->output_str[comb_lock->str_idx];
	comb_lock_set_symb_from_input(comb_lock);
	return 0;
}

int password_init(struct Password *password, char *str)
{
	if (password == NULL || str == NULL)
		return -EFAULT;
	password->buff_size = strlen(str) + 1;
	password->str = kmalloc(password->buff_size, GFP_KERNEL);
	if (password->str == NULL) {
		password->buff_size = 0;
		return -ENOMEM;
	}
	strcpy(password->str, str);
	return 0;
}

void password_cleanup(struct Password *password)
{
	if (password->str) {
		kfree(password->str);
		password->str = NULL;
		password->buff_size = 0;
	}
}

int password_set_str(struct Password *password, char *str)
{
	if (str == NULL || password->str == NULL)
		return -EFAULT;
	if (password->str == str)
		return 0;
	if (strlen(str) >= password->buff_size)
		return -ENOMEM;
	strcpy(password->str, str);
	return 0;
}

int comb_lock_add_password(struct Combination_lock *comb_lock,
			   struct Password *password)
{
	if (password == NULL || password->str == NULL) {
		return -EFAULT;
	}
	// temp solution, change to add list
	if (comb_lock->password == NULL) {
		if (strlen(password->str) != comb_lock->str_length)
			return -EINVAL;
		comb_lock->password = password;
		return 0;
	}
	return -EPERM;
}

int dictionary_init(struct Dictionary *dictionary, char *str)
{
	if (dictionary == NULL || str == NULL)
		return -EFAULT;
	dictionary->buff_size = strlen(str) + 1;
	dictionary->str = kmalloc(dictionary->buff_size, GFP_KERNEL);
	if (dictionary->str == NULL) {
		dictionary->buff_size = 0;
		return -ENOMEM;
	}
	strcpy(dictionary->str, str);
	dictionary->str_length = strlen(dictionary->str);
	return 0;
}

void dictionary_cleanup(struct Dictionary *dictionary)
{
	if (dictionary->str) {
		kfree(dictionary->str);
		dictionary->str = NULL;
		dictionary->buff_size = 0;
		dictionary->str_length = 0;
	}
}

int dictionary_set_str(struct Dictionary *dictionary, char *str)
{
	if (str == NULL || dictionary->str == NULL)
		return -EFAULT;
	if (dictionary->str == str)
		return 0;
	if (strlen(str) >= dictionary->buff_size)
		return -ENOMEM;
	strcpy(dictionary->str, str);
	dictionary->str_length = strlen(dictionary->str);
	return 0;
}

int comb_lock_set_dictionary(struct Combination_lock *comb_lock,
			     struct Dictionary *dictionary)
{
	if (dictionary == NULL || dictionary->str == NULL)
		return -EFAULT;
	comb_lock->dictionary = dictionary;
	return 0;
}

int combination_lock_init(struct Combination_lock *comb_lock,
			  uint16_t str_length)
{
	int err;
	printk(KERN_DEBUG MODULE_TAG "combination_lock_init init\n");
	if (comb_lock == NULL) {
		err = -EFAULT;
		goto err0;
	}
	if (str_length == 0) {
		err = -EINVAL;
		goto err0;
	}

	comb_lock->str_length = str_length;
	comb_lock->str_idx = 0;

	comb_lock->input_str = kmalloc(comb_lock->str_length + 1, GFP_KERNEL);
	if (comb_lock->input_str == NULL) {
		err = -ENOMEM;
		goto err0;
	}
	*comb_lock->input_str = '\0';

	comb_lock->mask_str = kmalloc(comb_lock->str_length + 1, GFP_KERNEL);
	if (comb_lock->mask_str == NULL) {
		err = -ENOMEM;
		goto err1;
	}
	*comb_lock->mask_str = '\0';
	comb_lock->output_str = comb_lock->input_str;

	comb_lock->dictionary_symb_indexes =
		kmalloc(comb_lock->str_length + 1, GFP_KERNEL);
	if (comb_lock->dictionary_symb_indexes == NULL) {
		err = -ENOMEM;
		goto err2;
	}
	*comb_lock->dictionary_symb_indexes = '\0';

	printk(KERN_DEBUG MODULE_TAG "loaded\n");
	return 0;

//err3:
//	kfree(comb_lock->dictionary_symb_indexes);
//	comb_lock->dictionary_symb_indexes = NULL;
err2:
	kfree(comb_lock->mask_str);
	comb_lock->mask_str = NULL;
err1:
	kfree(comb_lock->input_str);
	comb_lock->input_str = NULL;
err0:
	printk(KERN_ERR MODULE_TAG "failed to load\n");
	return err;
}

void combination_lock_destroy(struct Combination_lock *comb_lock)
{
	if (comb_lock->input_str) {
		kfree(comb_lock->input_str);
		comb_lock->input_str = NULL;
	}
	if (comb_lock->mask_str) {
		kfree(comb_lock->mask_str);
		comb_lock->mask_str = NULL;
	}
	if (comb_lock->dictionary_symb_indexes) {
		kfree(comb_lock->dictionary_symb_indexes);
		comb_lock->dictionary_symb_indexes = NULL;
	}
	comb_lock->str_length = 0;
	printk(KERN_DEBUG MODULE_TAG "combination_lock destroy\n");
}

int comb_lock_check_object_is_correct(struct Combination_lock *comb_lock)
{
	if (comb_lock == NULL || comb_lock->output_str == NULL ||
	    comb_lock->input_str == NULL ||
	    comb_lock->dictionary_symb_indexes == NULL ||
	    comb_lock->mask_str == NULL || comb_lock->dictionary == NULL ||
	    comb_lock->password == NULL) {
		return -EFAULT;
	}
	if (comb_lock->str_length == 0 ||
	    comb_lock->str_idx >= comb_lock->str_length ||
	    comb_lock->dictionary->str_length == 0 ||
	    strlen(comb_lock->password->str) != comb_lock->str_length ||
	    strlen(comb_lock->input_str) != comb_lock->str_length ||
	    (comb_lock->output_str == comb_lock->mask_str &&
	     strlen(comb_lock->mask_str) != comb_lock->str_length)) {
		return -EINVAL;
	}
	return 0;
}

void comb_lock_next_symb(struct Combination_lock *comb_lock)
{
	comb_lock->output_str[comb_lock->str_idx] = comb_lock->last_symb;

	if (comb_lock->str_idx >= comb_lock->str_length - 1)
		comb_lock->str_idx = 0;
	else
		comb_lock->str_idx++;

	comb_lock->last_symb = comb_lock->output_str[comb_lock->str_idx];

	comb_lock_set_symb_from_input(comb_lock);
}

static inline uint8_t
comb_lock_symb_next_idx_in_dictionary(struct Combination_lock *comb_lock)
{
	// the symbol index in the dictionary for the current symbol in the string
	uint8_t *idx_in_dict = &comb_lock_symb_get_idx_in_dictionary(comb_lock);

	if (*idx_in_dict >= comb_lock->dictionary->str_length - 1)
		*idx_in_dict = 0;
	else
		(*idx_in_dict)++;
	return *idx_in_dict;
}

static inline char
comb_lock_symb_next_symb_in_dictionary(struct Combination_lock *comb_lock)
{
	return comb_lock->dictionary
		->str[comb_lock_symb_next_idx_in_dictionary(comb_lock)];
}

inline uint8_t
comb_lock_symb_get_next_idx_in_dictionary(struct Combination_lock *comb_lock)
{
	// the symbol index in the dictionary for the current symbol in the string
	uint8_t idx_in_dict = comb_lock_symb_get_idx_in_dictionary(comb_lock);

	if (idx_in_dict >= comb_lock->dictionary->str_length - 1)
		idx_in_dict = 0;
	else
		(idx_in_dict)++;
	return idx_in_dict;
}

inline char
comb_lock_symb_get_next_symb_in_dictionary(struct Combination_lock *comb_lock)
{
	return comb_lock->dictionary
		->str[comb_lock_symb_get_next_idx_in_dictionary(comb_lock)];
}

void comb_lock_change_symb_next(struct Combination_lock *comb_lock)
{
	comb_lock->input_str[comb_lock->str_idx] =
		comb_lock_symb_next_symb_in_dictionary(comb_lock);
	comb_lock_set_symb_from_input(comb_lock);
	if (comb_lock->output_str != comb_lock->mask_str)
		comb_lock->last_symb =
			comb_lock->output_str[comb_lock->str_idx];
}

void comb_lock_prev_symb(struct Combination_lock *comb_lock)
{
	comb_lock->output_str[comb_lock->str_idx] = comb_lock->last_symb;

	if (comb_lock->str_idx == 0)
		comb_lock->str_idx = comb_lock->str_length - 1;
	else
		comb_lock->str_idx--;

	comb_lock->last_symb = comb_lock->output_str[comb_lock->str_idx];

	comb_lock_set_symb_from_input(comb_lock);
}

static inline uint8_t
comb_lock_symb_prev_idx_in_dictionary(struct Combination_lock *comb_lock)
{
	// the symbol index in the dictionary for the current symbol in the string
	uint8_t *idx_in_dict = &comb_lock_symb_get_idx_in_dictionary(comb_lock);

	if (*idx_in_dict == 0)
		*idx_in_dict = comb_lock->dictionary->str_length - 1;
	else
		(*idx_in_dict)--;
	return *idx_in_dict;
}

static inline char
comb_lock_symb_prev_symb_in_dictionary(struct Combination_lock *comb_lock)
{
	return comb_lock->dictionary
		->str[comb_lock_symb_prev_idx_in_dictionary(comb_lock)];
}

inline uint8_t
comb_lock_symb_get_prev_idx_in_dictionary(struct Combination_lock *comb_lock)
{
	// the symbol index in the dictionary for the current symbol in the string
	uint8_t idx_in_dict = comb_lock_symb_get_idx_in_dictionary(comb_lock);

	if (idx_in_dict == 0)
		idx_in_dict = comb_lock->dictionary->str_length - 1;
	else
		(idx_in_dict)--;
	return idx_in_dict;
}

inline char
comb_lock_symb_get_prev_symb_in_dictionary(struct Combination_lock *comb_lock)
{
	return comb_lock->dictionary
		->str[comb_lock_symb_get_prev_idx_in_dictionary(comb_lock)];
}

void comb_lock_change_symb_prev(struct Combination_lock *comb_lock)
{
	comb_lock->input_str[comb_lock->str_idx] =
		comb_lock_symb_prev_symb_in_dictionary(comb_lock);

	comb_lock_set_symb_from_input(comb_lock);

	if (comb_lock->output_str != comb_lock->mask_str)
		comb_lock->last_symb =
			comb_lock->output_str[comb_lock->str_idx];
}

static bool comb_lock_try_password(struct Combination_lock *comb_lock)
{
	if (0 == strcmp(comb_lock->input_str, comb_lock->password->str)) {
		return true;
	} else {
		return false;
	}
}

bool comb_lock_try_open(struct Combination_lock *comb_lock)
{
	if (comb_lock_try_password(comb_lock)) {
		comb_lock_set_input_str(comb_lock, 0);
		comb_lock_set_symb_from_input(comb_lock);
		return true;
	} else {
		return false;
	}
}
#endif
