
#ifndef _COMBINATION_LOCK_DISPLAY_C_
#define _COMBINATION_LOCK_DISPLAY_C_

#include "combination_lock_display.h"
#include "fonts.h"
#include "st7735.h"

extern inline void lcd_1_8_update_screen(void);
extern void lcd_1_8_draw_pixel(u16 x, u16 y, u16 color);
extern void lcd_1_8_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color);
extern void lcd_1_8_fill_screen(u16 color);
extern void lcd_1_8_put_char(u16 x, u16 y, char ch, FontDef font, u16 color,
			     u16 bgcolor);
extern void lcd_1_8_put_str(u16 x, u16 y, const char *str, FontDef font,
			    u16 color, u16 bgcolor);

static int bgcolor = COLOR_BLACK;
static int textcolor = COLOR_CYAN;

void comb_lock_print_start_screen(struct Combination_lock *comb_lock)
{
	lcd_1_8_fill_screen(bgcolor);
}

void comb_lock_print_str(struct Combination_lock *comb_lock)
{
	if (comb_lock->output_str) {
		printk(KERN_NOTICE MODULE_TAG "LCD: %s\n",
		       comb_lock->output_str);
		//	lcd_1_8_fill_rectangle(0, 25, 11, 25 + 18, bgcolor);
		//	lcd_1_8_fill_rectangle(0, 70, 11, 70 + 18, bgcolor);
		lcd_1_8_fill_screen(bgcolor);

		lcd_1_8_put_char(
			comb_lock->str_idx * 16 + 3, 25,
			comb_lock_symb_get_prev_symb_in_dictionary(comb_lock),
			Font_11x18, textcolor, bgcolor);
		lcd_1_8_put_str(0, 45, comb_lock->output_str, Font_16x26,
				textcolor, bgcolor);
		lcd_1_8_put_char(
			comb_lock->str_idx * 16 + 3, 70,
			comb_lock_symb_get_next_symb_in_dictionary(comb_lock),
			Font_11x18, textcolor, bgcolor);
		lcd_1_8_update_screen();
	}
}

void comb_lock_print_symb(struct Combination_lock *comb_lock)
{
	if (comb_lock->output_str) {
		printk(KERN_NOTICE MODULE_TAG "LCD: %c\n",
		       comb_lock->output_str[comb_lock->str_idx]);
	}
}

void comb_lock_print(struct Combination_lock *comb_lock)
{
	comb_lock_print_str(comb_lock);
}

void comb_lock_print_lock_is_open(struct Combination_lock *comb_lock)
{
	printk(KERN_NOTICE MODULE_TAG "LCD: lock is open\n");
	bgcolor = COLOR_GREEN;
	textcolor = COLOR_BLUE;
	lcd_1_8_fill_screen(bgcolor);
	lcd_1_8_update_screen();
}

void comb_lock_print_lock_is_close(struct Combination_lock *comb_lock)
{
	printk(KERN_NOTICE MODULE_TAG "LCD: lock is open\n");
	bgcolor = COLOR_BLACK;
	lcd_1_8_fill_screen(bgcolor);
	lcd_1_8_update_screen();
}

#endif
