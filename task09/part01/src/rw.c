/*******************************************************************************
* Copyright (c) 2015 Song Yang @ ittraining
* 
* All rights reserved.
* This program is free to use, but the ban on selling behavior.
* Modify the program must keep all the original text description.
*
* Email: onionys@ittraining.com.tw
* Blog : http://blog.ittraining.com.tw
*******************************************************************************/
#include <linux/module.h>
#include <linux/init.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include <linux/timer.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/module.h> 
#include <linux/delay.h> 
#include <linux/kthread.h>
#include <linux/jiffies.h> 
#include <linux/semaphore.h>
#include <linux/rwlock.h>

#define PROC_DIRECTORY  "Messages_concurrency"
#define MESSAGE_FILENAME   "message"
#define BUFFER_SIZE     100
#define MODULE_TAG      "Messages_concurrency "

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ITtraining.com.tw");
MODULE_DESCRIPTION("Messages_concurrency.");

typedef struct _Message_thread
{
	char message_text[256];
	uint32_t interval;
	rwlock_t lock;
} Message_thread;

static Message_thread * messages;
static uint16_t msgs_count = 20;
static uint16_t msgs_idx;


static char * buffer_read;

int print_message(Message_thread * message)
{
	printk( "Msg: %s\n", message->message_text);	
	return 0;
}

int thread_print( void* data ) { 
	Message_thread * msg= (Message_thread *)data;
	uint32_t interval = msg->interval;
	while(1)
	{
		read_lock(&msg->lock);
		print_message(msg);
		interval = msg->interval;
		read_unlock(&msg->lock);
		
		msleep( interval ); 
	}
   return 0;
}

int parse_message(char * str, Message_thread * message)
{
	char * token = str;
	char * endptr = str;
	uint32_t count;
	ktime_t sec;

	if(str == NULL || message == NULL)
	{
		return -EFAULT;
	}
	
	endptr = strchr(token, ':');

	if(endptr == NULL)
	{
		return -EINVAL;
	}
	count = endptr - token;
	if(count > 256)
		return -ENOMEM;
		
	strncpy(message->message_text, token, count);
	token = endptr + 1;
	
	sec = simple_strtol(token, &endptr, 10);
	printk(KERN_DEBUG "sec in parse %lld\n", sec);
	if(endptr == token)
	{
		printk(KERN_DEBUG "ERR in parse num\n");
		return -EINVAL;
	}
	message->interval = sec * 1000;
		
	return 0;
}

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file_message;

static ssize_t message_write(struct file *file_p, const char __user *buffer,
		size_t length, loff_t *offset);


static ssize_t message_write(struct file *file_p, const char __user *buffer,
		size_t length, loff_t *offset)
{
	//Message_thread msg;
    size_t left;
    int err;
	printk(KERN_DEBUG "in message_write\tlen: %zu\n", length);
	if (length == 0)
		return 0;
		
    if (length + 1 > BUFFER_SIZE)
    {
		printk(KERN_WARNING MODULE_TAG "reduse message length from %lu to %u chars\n",
			length, BUFFER_SIZE);
        length = BUFFER_SIZE;
    }
		
	left = copy_from_user(buffer_read, buffer, length);
	buffer_read[length] = '\0';

	printk(KERN_DEBUG "in write message\tstr: %s\n", buffer_read);
	if(msgs_idx >= msgs_count)
	{
		printk(KERN_ERR MODULE_TAG "failed, buffer fulled%s \n", buffer_read);
		return 0;
	}
		
	write_lock(&(messages + msgs_idx)->lock);
	err = parse_message(buffer_read, messages + msgs_idx);
	if(0 > err)
	{
		printk(KERN_ERR MODULE_TAG "failed to parse message %s \n", buffer_read);
		return err;
	}
	
	//memcpy(messages + msgs_idx, &msg, sizeof(msg));
	write_unlock(&(messages + msgs_idx)->lock);

	printk(KERN_DEBUG "in message_write\tmessage: %s\n", messages[msgs_idx].message_text);
	printk(KERN_DEBUG "in message_write\tinterval: %d\n\n", messages[msgs_idx].interval);

	kthread_run( thread_print, (void*)(messages + msgs_idx), "message_%d", msgs_idx);
      
	++msgs_idx;
	
    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write message %lu from %lu chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %lu chars\n", length);

    return length;
}

static struct file_operations proc_result_fops = {
    .write = message_write
};

static int create_proc_info(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file_message = proc_create(MESSAGE_FILENAME, S_IFREG | S_IRUGO | S_IWUGO,
						proc_dir, &proc_result_fops);
    if (NULL == proc_file_message)
        return -EFAULT;
        

    return 0;
}

static void cleanup_proc_info(void)
{
    if (proc_file_message)
    {
        remove_proc_entry(MESSAGE_FILENAME, proc_dir);
        proc_file_message = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}

static int create_buffers(void)
{
	buffer_read = kmalloc(BUFFER_SIZE, GFP_KERNEL);
	if (NULL == buffer_read)
		return -ENOMEM;
	messages = kmalloc(msgs_count * sizeof(*messages), GFP_KERNEL);
	if (NULL == messages)
		return -ENOMEM;

	return 0;
}

static void cleanup_buffer(void)
{
    if (buffer_read)
    {
        kfree(buffer_read);
        buffer_read = NULL;
    }
    if (messages)
    {
        kfree(messages);
        messages = NULL;
    }
}

/*
 * INIT MODULE
 * */
int init_module(void)
{
	printk("start thread_message module\n");

	create_buffers();
	create_proc_info();

	printk("END: init_module() \n");
	return 0;
}

/*
 * CLEANUP MODULE
 * */
void cleanup_module(void)
{
	
	cleanup_proc_info();
	cleanup_buffer();
	printk("Goodbye\n");
}
