
#ifndef _STR_MANIP_H_
#define _STR_MANIP_H_

#include <linux/kernel.h>
#include <linux/ctype.h>

char *strrev(char *str_begin, char *str_end);

char * flip_words(char * str, size_t length);

char *strtoup(char * str);

#endif
