
#ifndef _STR_MANIP_C_
#define _STR_MANIP_C_

#include "str_manip.h"

char *strrev(char *str_begin, char *str_end)
{
	char *p1, *p2;
	char ch;
	
	if (! str_begin || ! *str_end || str_begin >= str_end)
		return str_begin;
		
	for (p1 = str_begin, p2 = str_end; p2 > p1; ++p1, --p2)
	{
		ch = *p1;
		*p1 = *p2;
		*p2 = ch;
	}
	
	return str_begin;
}

char * flip_words(char * str, size_t length)
{
	char * beg_word;
	char * cur_ch;
	bool is_word = false;
		
	for (cur_ch = str; cur_ch < str + length; ++cur_ch)
	{
		if (*cur_ch == ' ' || *cur_ch == '\n'
		 || *cur_ch == '\t' || *cur_ch == '\0')
		{
			if (is_word)
			{
				is_word = false;
				strrev(beg_word, cur_ch - 1);
			}
		}
		else
		{
			if(!is_word)
			{
				is_word = true;
				beg_word = cur_ch;
			}
		}
	}
	return str;
}

char *strtoup(char * str)
{
	char * cur;
	for(cur = str; *cur != '\0'; ++cur)
	{
		*cur = toupper(*cur);
	}
	return str;
}
#endif
