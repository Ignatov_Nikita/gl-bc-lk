#!/bin/bash

TARGET=str_translation

ssh -p 8022 user@localhost "
sudo -S dmesg -c > /dev/null
sudo insmod str_translation.ko
echo 1 | sudo tee /sys/class/user/str_translate_mode
cat /sys/class/user/str_translate_mode
echo "Hello Global word" > /proc/user/string
cat /proc/user/string
echo 2 | sudo tee /sys/class/user/str_translate_mode
echo "Hello Global word" > /proc/user/string
cat /proc/user/string
echo 0 | sudo tee /sys/class/user/str_translate_mode
echo "Hello Global word" > /proc/user/string
cat /proc/user/string
sudo rmmod str_translation.ko
dmesg"

