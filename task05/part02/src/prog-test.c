
#ifndef PROG_TEST_C
#define PROG_TEST_C
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
	char ans[1000];
	int length;
	char * exchange_rates1 = "UAH=1;EUR=30;RUB=0.4";
	char * exchange_rates2 = "EUR=30.56;RUB=0.38;USD=28.03";
	char * exchange_operation1 = "EUR=100:RUB";
	char * exchange_operation2 = "EUR=100:USD";
	printf("Current rate\n");

	int f_ex_rates = open("/sys/class/Currency_converter/curr_rates", O_RDWR);
	if(0 > f_ex_rates)
	{
		printf("Err fopen ex_rates\n");
		return -1;
	}
	
	int f_ex_operat = open("/proc/Currency_converter/exchange", O_RDWR);
	if(0 > f_ex_operat)
	{
		printf("Err fopen f_ex_operat\n");
		return -1;
	}
	
	int f_ex_log = open("/proc/Currency_converter/conversion_log", O_RDONLY);
	if(0 > f_ex_log)
	{
		printf("Err fopen f_ex_log\n");
		return -1;
	}
	
	printf("Adding currency rates: %s\n", exchange_rates1);
	write(f_ex_rates, exchange_rates1, strlen(exchange_rates1) + 1);
	if(0 > f_ex_rates)
	{
		printf("Err write\n");
	}
	
	lseek(f_ex_rates, 0, SEEK_SET);
	length = read(f_ex_rates, ans, 100);
	if(0 > f_ex_rates)
	{
		printf("Err read\n");
	}
	ans[length - 1] = '\0';
	printf("Current currency rates: %s\n", ans);
	
	printf("Exchange request: %s\n", exchange_operation1);
	
	write(f_ex_operat, exchange_operation1, strlen(exchange_operation1) + 1);
	if(0 > f_ex_operat)
	{
		printf("Err write: %s\n", exchange_operation1);
	}
	
	lseek(f_ex_operat, 0, SEEK_SET);
	length = read(f_ex_operat, ans, 100);
	if(0 > f_ex_operat)
	{
		printf("Err read\n");
	}
	ans[length - 1] = '\0';
	printf("Exchange response: %s\n", ans);
	

	printf("Adding new currency rates: %s\n", exchange_rates2);
	write(f_ex_rates, exchange_rates2, strlen(exchange_rates2) + 1);
	if(0 > f_ex_rates)
	{
		printf("Err write: %s\n", exchange_rates2);
	}
	
	lseek(f_ex_rates, 0, SEEK_SET);
	length = read(f_ex_rates, ans, 100);
	if(0 > f_ex_rates)
	{
		printf("Err read\n");
	}
	ans[length - 1] = '\0';
	printf("Current currency rates: %s\n", ans);
	
	printf("Exchange request: %s\n", exchange_operation2);
	write(f_ex_operat, exchange_operation2, strlen(exchange_operation2) + 1);
	if(0 > f_ex_operat)
	{
		printf("Err write: %s\n", exchange_operation2);
	}
	
	lseek(f_ex_operat, 0, SEEK_SET);
	length = read(f_ex_operat, ans, 100);
	if(0 > f_ex_operat)
	{
		printf("Err read\n");
	}
	ans[length - 1] = '\0';
	printf("Exchange response: %s\n", ans);
	
	
	length = read(f_ex_log, ans, 1000);
	if(0 > f_ex_log)
	{
		printf("Err read\n");
	}
	ans[length - 1] = '\0';
	printf("Conversion logging:\n%s\n", ans);
	
	close(f_ex_rates);
	close(f_ex_operat);
	close(f_ex_log);
	return 0;
}
#endif
