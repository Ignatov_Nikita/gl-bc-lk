
#ifndef CONVER_LOG_C
#define CONVER_LOG_C
#include "conversion_logging.h"

static char *conversion_loggining;
static char *log_pos;
static size_t conversion_loggining_len;
static size_t conversion_loggining_size;

inline const char * get_conv_log(void)
{
	return conversion_loggining;
}

inline size_t get_conv_log_len(void)
{
	return conversion_loggining_len;
}

int conv_log_store(const char * str)
{
	size_t len;
	
	if(NULL == str)
		return -EFAULT;
		
	len = strlen(str);
	
	if(conversion_loggining_size < conversion_loggining_len + 1 + len + 1)
		return -ENOMEM;
		
	printk(KERN_DEBUG "in log_store\tstr: %s\n", str);
	printk(KERN_DEBUG "\tlog: %s\n", conversion_loggining);
	printk(KERN_DEBUG "\tpos: %s\n", log_pos);
	strcpy(log_pos, str);
	log_pos += len;
	*log_pos = '\n';
	*(++log_pos) = '\0';
	conversion_loggining_len += len + 1;
	return 0;
}


int create_conv_log(size_t length)
{
	if(0 == length)
		return -EINVAL;
		
	conversion_loggining = kmalloc(length, GFP_KERNEL);
	if (NULL == conversion_loggining)
		return -ENOMEM;
	
	conversion_loggining_size = length;
	conversion_loggining_len = 0;
	log_pos = conversion_loggining;
	*log_pos = '\0';
	return 0;
}

void free_conv_log(void)
{
    if (conversion_loggining)
    {
        kfree(conversion_loggining);
        conversion_loggining = NULL;
    }
}

#endif

