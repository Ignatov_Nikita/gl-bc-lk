
#ifndef _RW_C_
#define _RW_C_

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/init.h>
#include "currency_exchange.h"
#include "conversion_logging.h"


MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Nikita Ignatov <nikitasinys@gmail.com>");
MODULE_DESCRIPTION("Currency converter using procfs to convert and sysfs to set currency coefficient");
MODULE_VERSION("0.1");


#define MODULE_TAG      "currency_converter "
#define PROC_DIRECTORY  "Currency_converter"
#define EXCHANGE_FILENAME   "exchange"
#define CONVER_LOG_FILENAME   "conversion_log"
#define SYS_DIRECTORY  "Currency_converter"
#define BUFFER_SIZE     100
#define CONVER_LOG_SIZE     1000

static char *exchange_rates_rsp;
static size_t exchange_rates_rsp_len;
static bool exchange_rates_rsp_is_relevant;

static char *exchange_request;
static char *exchange_response;
static size_t exchange_operation_rsp_len;
static bool response_is_relevant;

static Exchange_rate_Array exchange_rates;
static Exchange_operation exchange_operation;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file_exchange;
static struct proc_dir_entry *proc_file_conver_log;

static int exch_op_rsp_read(struct file *file_p, char __user *buffer, 
		size_t length, loff_t *offset);
static int exch_op_req_write(struct file *file_p, const char __user *buffer, 
		size_t length, loff_t *offset);


static ssize_t curr_rates_show( struct class *class, 
			struct class_attribute *attr, char *buf);
static ssize_t curr_rates_store( struct class *class, 
			struct class_attribute *attr, const char *buf, size_t count);
		
static ssize_t curr_rates_show( struct class *class, 
		struct class_attribute *attr, char *buffer )
{
	int err;

	printk(KERN_DEBUG "in read rates length: %zu\n\n", strlen(buffer));
	
	if (!exchange_rates_rsp_is_relevant)
	{
		printk(KERN_DEBUG "in read rates\trate changed\n");
		if(0 < exchange_rates.length)
		{
			err = pack_exchange_rates(exchange_rates_rsp, BUFFER_SIZE, &exchange_rates);
			if(0 > err)
			{
				printk(KERN_ERR MODULE_TAG "failed to pack %s \n", exchange_rates_rsp);
				return 0;
			}
			
			printk(KERN_DEBUG "in read rates\tstr_res: %s\n", exchange_rates_rsp);
			
			exchange_rates_rsp_len = strlen(exchange_rates_rsp) + 1;
			exchange_rates_rsp_is_relevant = true;
		}
		else
		{
			return 0;
		}
	}
	
	strncpy(buffer, exchange_rates_rsp, exchange_rates_rsp_len);

printk(KERN_DEBUG "in read rates\tread length: %zu\n\n", exchange_rates_rsp_len);
printk(KERN_DEBUG "in read rates\tcurr: %s\n", genradix_ptr(&exchange_rates.array, 0)->currency_name);
printk(KERN_DEBUG "in read rates\tcoef: %ld\n\n", genradix_ptr(&exchange_rates.array, 0)->convert_factor);
			

	printk(KERN_DEBUG "in read rates\tstr_usr: %s\n", buffer);
	
	printk(KERN_NOTICE MODULE_TAG "read rates %u chars\n", exchange_rates_rsp_len);

	return exchange_rates_rsp_len;
}

static ssize_t curr_rates_store( struct class *class,
		struct class_attribute *attr, const char *buffer, size_t length )
{
	int err;

	if (length > BUFFER_SIZE)
	{
printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
		length = BUFFER_SIZE;
	}
	
	exchange_rates_rsp_len = 0;
	exchange_rates_rsp_is_relevant = false;
	
	printk(KERN_DEBUG "in write rates\n");
	  
	err = parse_exchange_rates(buffer, &exchange_rates);
	if(0 > err)
	{
		printk(KERN_ERR MODULE_TAG "failed to parse rates %s \n", buffer);
		return 0;
	}

printk(KERN_DEBUG "in write rates\tcurr: %s\n", genradix_ptr(&exchange_rates.array, 0)->currency_name);
printk(KERN_DEBUG "in write rates\tcoef: %ld\n\n", genradix_ptr(&exchange_rates.array, 0)->convert_factor);
	  
	printk(KERN_NOTICE MODULE_TAG "written %u chars\n", length);

	return length;
}

CLASS_ATTR_RW( curr_rates );
static struct class *curr_convert_class;

static int exch_op_rsp_read(struct file *file_p, char __user *buffer,
		size_t length, loff_t *offset)
{
	size_t left;
	int err;

	printk(KERN_DEBUG "in read oper length: %zu\n\n", length);
	
	if (!response_is_relevant)
	{
		err = perform_exchange_operation(&exchange_operation);
		if(0 > err)
		{
		printk(KERN_ERR MODULE_TAG "failed to perform_exchange_operation\n");
			return err;
		}
		err = pack_Exchange_operation_ans(exchange_response, BUFFER_SIZE, &exchange_operation);
		if(0 > err)
		{
		printk(KERN_ERR MODULE_TAG "failed to pack oper %s \n", exchange_request);
			return err;
		}
		conv_log_store(exchange_response);
		
		printk(KERN_DEBUG "in read oper\tstr_res: %s\n", exchange_response);
		
		response_is_relevant = true;
		exchange_operation_rsp_len = strlen(exchange_response) + 1;
	}
	if (*offset > exchange_operation_rsp_len)
		*offset = exchange_operation_rsp_len;
	
	if (length > exchange_operation_rsp_len - *offset)
		length = exchange_operation_rsp_len - *offset;

printk(KERN_DEBUG "in read oper\tcurr: %s\n", genradix_ptr(&exchange_rates.array, 0)->currency_name);
printk(KERN_DEBUG "in read oper\tcoef: %ld\n\n", genradix_ptr(&exchange_rates.array, 0)->convert_factor);
	
	if(length == 0)
		return 0;
		
	left = copy_to_user(buffer, exchange_response, length);
	
	*offset = length - left;
	printk(KERN_DEBUG "in read oper\tstr_usr: %s\n", buffer);
	

	if (left)
		printk(KERN_ERR MODULE_TAG "failed to read oper %u from %u chars\n", left, length);
	else
		printk(KERN_NOTICE MODULE_TAG "read oper %u chars\n", length);

	return length - left;
}

static int exch_op_req_write(struct file *file_p, const char __user *buffer,
		size_t length, loff_t *offset)
{
    size_t left;
    int err;

	printk(KERN_DEBUG "in write oper\tlen: %zu\n", length);
	if (length == 0)
		return 0;
		
    if (length + 1 > BUFFER_SIZE)
    {
printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
        length = BUFFER_SIZE;
    }
		
	left = copy_from_user(exchange_request, buffer, length);
	exchange_request[length] = '\0';
    response_is_relevant = false;

	printk(KERN_DEBUG "in write oper\tstr: %s\n", exchange_request);
	
	err = parse_Exchange_operation(exchange_request, &exchange_operation, &exchange_rates);
	if(0 > err)
	{
		printk(KERN_ERR MODULE_TAG "failed to parse oper %s \n", exchange_request);
		return err;
	}
	conv_log_store(exchange_request);

printk(KERN_DEBUG "in write oper\tcurr: %s\n", genradix_ptr(&exchange_rates.array, 0)->currency_name);
printk(KERN_DEBUG "in write oper\tcoef: %ld\n\n", genradix_ptr(&exchange_rates.array, 0)->convert_factor);
      
    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write oper %u from %u chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %u chars\n", length);

    return length;
}

static struct proc_ops proc_exchange_fops = {
    .proc_read  = exch_op_rsp_read,
    .proc_write = exch_op_req_write
};

static int conver_log_read(struct file *file_p, char __user *buffer,
		size_t length, loff_t *offset)
{
	size_t log_len;
	size_t left;

	printk(KERN_DEBUG "in read log length: %zu\n\n", length);
	
	log_len = get_conv_log_len() + 1;
	
	if (*offset > log_len)
		*offset = log_len;
	
	if (length > log_len - *offset)
		length = log_len - *offset;

printk(KERN_DEBUG "\n\nin read log\tread str len: %zu\n", log_len);
printk(KERN_DEBUG "in read log\tread offset: %llu\n", *offset);
printk(KERN_DEBUG "in read log\tread length: %zu\n\n", length);
	
	if(length == 0)
		return 0;
		
	left = copy_to_user(buffer, get_conv_log(), length);
	
	*offset = length - left;
	
	printk(KERN_DEBUG "in read log\tleft: %zu\n\n", left);


	if (left)
		printk(KERN_ERR MODULE_TAG "failed to read log %u from %u chars\n", left, length);
	else
		printk(KERN_NOTICE MODULE_TAG "read log %u chars\n", length);

	return length - left;
}

static struct proc_ops proc_conver_log_fops = {
    .proc_read  = conver_log_read
};

static int create_buffers(void)
{
	exchange_request = kmalloc(BUFFER_SIZE, GFP_KERNEL);
	if (NULL == exchange_request)
		return -ENOMEM;
		
	exchange_response = kmalloc(BUFFER_SIZE, GFP_KERNEL);
	if (NULL == exchange_response)
		return -ENOMEM;
		
	exchange_rates_rsp = kmalloc(BUFFER_SIZE, GFP_KERNEL);
	if (NULL == exchange_rates_rsp)
		return -ENOMEM;
		
	exchange_rates_rsp_len = 0;
	exchange_rates_rsp_is_relevant = false;
	exchange_operation_rsp_len = 0;
	response_is_relevant = false;
	return 0;
}

static void cleanup_buffer(void)
{
    if (exchange_request)
    {
        kfree(exchange_request);
        exchange_request = NULL;
    }
    if (exchange_response)
    {
        kfree(exchange_response);
        exchange_response = NULL;
    }
    if (exchange_rates_rsp)
    {
        kfree(exchange_rates_rsp);
        exchange_rates_rsp = NULL;
    }
}


static int create_proc_info(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file_exchange = proc_create(EXCHANGE_FILENAME, S_IFREG | S_IRUGO | S_IWUGO,
						proc_dir, &proc_exchange_fops);
    if (NULL == proc_file_exchange)
        return -EFAULT;
        
    proc_file_conver_log = proc_create(CONVER_LOG_FILENAME, S_IFREG | S_IRUGO | S_IWUGO,
						proc_dir, &proc_conver_log_fops);
    if (NULL == proc_file_conver_log)
        return -EFAULT;

    return 0;
}

static void cleanup_proc_info(void)
{
    if (proc_file_exchange)
    {
        remove_proc_entry(EXCHANGE_FILENAME, proc_dir);
        proc_file_exchange = NULL;
    }
    if (proc_file_conver_log)
    {
        remove_proc_entry(CONVER_LOG_FILENAME, proc_dir);
        proc_file_conver_log = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}

static int create_sys_info(void)
{
	int err;
	
	curr_convert_class = class_create( THIS_MODULE, SYS_DIRECTORY );
	if( IS_ERR( curr_convert_class ) ) 
		printk( "bad class create\n" );
	err = class_create_file( curr_convert_class, &class_attr_curr_rates );
	if (err)
		return -EFAULT;
		
    return 0;
}

static void cleanup_sys_info(void)
{
	class_remove_file( curr_convert_class, &class_attr_curr_rates );
	class_destroy( curr_convert_class );
}


static int __init example_init(void)
{
    int err;

	printk(KERN_NOTICE MODULE_TAG "Converter start\n");
    err = create_buffers();
	if (err)
		goto error;

	printk(KERN_NOTICE MODULE_TAG "bef file\n");
    err = create_proc_info();
	if (err)
		goto error;
		
	printk(KERN_NOTICE MODULE_TAG "bef array init\n");
	genradix_init(&exchange_rates.array);
	printk(KERN_NOTICE MODULE_TAG "bef array alloc\n");
	err = genradix_prealloc(&exchange_rates.array, 4, GFP_KERNEL);
	if (err)
		goto error;
        
	printk(KERN_NOTICE MODULE_TAG "bef sys-class\n");
        
	err = create_sys_info();
	if (err)
		goto error;
        
	err = create_conv_log(CONVER_LOG_SIZE);
	if (err)
        printk(KERN_ERR MODULE_TAG "failed to create conversion logging\n");

    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_proc_info();
    cleanup_sys_info();
    cleanup_buffer();
    free_conv_log();
    genradix_free(&exchange_rates.array);
    return err;
}


static void __exit example_exit(void)
{
    cleanup_proc_info();
    cleanup_sys_info();
    cleanup_buffer();
    free_conv_log();
    genradix_free(&exchange_rates.array);
    printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(example_init);
module_exit(example_exit);

#endif
