
#ifndef CURRENCY_EXCHANGE_H
#define CURRENCY_EXCHANGE_H

#include <linux/kernel.h>
#include <linux/ctype.h>
#include <linux/string.h>
#include <linux/generic-radix-tree.h>
#include <linux/gfp.h>
#include "list.h"

typedef struct _Exchange_rate
{
	char currency_name[4];
	long convert_factor;
} Exchange_rate;

typedef struct _Exchange_operation
{
	Exchange_rate * sell;
	long sell_amount;
	Exchange_rate * buy;
	long buy_amount;
} Exchange_operation;

typedef ARRAY(Exchange_rate) Exchange_rate_Array; 

/* NOTE:
 * success: return 0
 * error: return num < 0
 */
int perform_exchange_operation(Exchange_operation * exchange_operation);

/* NOTE:
 * str should contain \0
 * success: return 0
 * error: return num < 0
 */
int parse_Exchange_rate(char * str, Exchange_rate_Array * exchange_rates_arr);

/* NOTE:
 * str should contain \0
 * success: return 0
 * error: return num < 0
 */
int parse_exchange_rates(const char * exchange_rates,
		Exchange_rate_Array * exchange_rates_arr);

/* NOTE:
 * success: return 0
 * error: return num < 0
 */
int pack_Exchange_rate(char *str, size_t str_len, Exchange_rate * curr_rate);

/* NOTE:
 * str should contain \0
 * success: return 0
 * error: return num < 0
 */
int pack_exchange_rates(char *str, size_t str_len,
		Exchange_rate_Array * exchange_rates);

/* NOTE:
 * str should contain \0
 * success: return 0
 * error: return num < 0
 */
int parse_Exchange_operation(char * str, Exchange_operation * exchange_operation,
		Exchange_rate_Array * exchange_rates);

/* NOTE:
 * success: return number of characters processed
 * error: return num < 0
 */
int pack_Exchange_operation_ans(char * str, size_t str_len,
			Exchange_operation * exchange_operation);
#endif
