
#ifndef CONVER_LOG_H
#define CONVER_LOG_H

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <uapi/asm-generic/errno-base.h>

inline const char * get_conv_log(void);

inline size_t get_conv_log_len(void);


int conv_log_store(const char * str);


int create_conv_log(size_t length);

void free_conv_log(void);

#endif

