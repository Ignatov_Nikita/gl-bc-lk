
#ifndef LIST_H
#define LIST_H

#include <linux/generic-radix-tree.h>
#include <linux/gfp.h>

//typedef GENRADIX(Exchange_rate) Exchange_rate_Genrdx;

#define ARRAY(_type) 		\
struct 				\
{							\
	GENRADIX(_type) array;	\
	size_t length;			\
}

#define array_iter_last(_array)			\
genradix_iter_init((_array)->array, (_array)->length)


#define array_for_each_from(_array, _iter, _p, _start)	\
	for (_iter = genradix_iter_init(&(_array->array), _start), \
		_p = genradix_iter_peek(&_iter, &(_array->array));	\
	     _iter.pos < (_array)->length;	\
	     genradix_iter_advance(&_iter, &(_array->array)), \
	     _p = genradix_iter_peek(&_iter, &(_array->array)))

#endif

