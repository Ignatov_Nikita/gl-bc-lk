#!/bin/bash
sudo rmmod currency_converter.ko
sudo dmesg -c >/dev/null
sudo insmod currency_converter.ko 
sudo printf "UAH=1;EUR=30;RUB=0.4" > /sys/class/Currency_converter/curr_rates
echo "Currency rates:"
sudo cat /sys/class/Currency_converter/curr_rates
echo
echo
echo "Request:"
echo "EUR=93.45:RUB"
echo
sudo printf "EUR=93.45:RUB" > /proc/Currency_converter/exchange
echo "Response:"
sudo cat /proc/Currency_converter/exchange
echo
echo
echo "Adding new rate:"
sudo printf "EUR=30.02;USD=28" > /sys/class/Currency_converter/curr_rates
sudo cat /sys/class/Currency_converter/curr_rates
echo
echo
echo "Request:"
echo "UAH=50:USD"
sudo printf "UAH=50:USD" > /proc/Currency_converter/exchange
echo
echo "Response:"
sudo cat /proc/Currency_converter/exchange
echo
echo
echo "History conversion:"
sudo cat /proc/Currency_converter/conversion_log
echo
#dmesg
