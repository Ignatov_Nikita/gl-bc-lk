
#ifndef CURRENCY_EXCHANGE_C
#define CURRENCY_EXCHANGE_C

#include "currency_exchange.h"


/* NOTE:
 * success: return 0
 * error: return num < 0
 */
int perform_exchange_operation(Exchange_operation * exchange_operation)
{
	if(exchange_operation == NULL || exchange_operation->buy == NULL ||
		exchange_operation->sell == NULL)
	{
		return -EFAULT;
	}
	if(0 == exchange_operation->buy->convert_factor)
		return -EINVAL;
		
	exchange_operation->buy_amount = (exchange_operation->sell_amount * 
				exchange_operation->sell->convert_factor) / 
					exchange_operation->buy->convert_factor;
	return 0;
}

/* NOTE:
 * <!> str_double will be modified and restored
 * str_double should contain \0
 * str_double can contain '.' or ','
 * Example 12.4 set to 1240
 * success: return number of characters processed
 * error: return num < 0
 */
static int parse_str_double(long * dest_long, char * str_double)
{
	char * token = str_double;
	char * endptr;
	long num;
	long numL;
	long count;
	
	printk(KERN_DEBUG "in parse double\tstr: %s\n", str_double);

	if(str_double == NULL || dest_long == NULL)
	{
      printk(KERN_DEBUG "ERR in parse double\n");
		return -EFAULT;
	}

	/*
	*	parse digits before dot
	*/

	num = 100 * simple_strtol(token, &endptr, 10);
	if(endptr == token)
	{
      printk(KERN_DEBUG "ERR in parse double\n");
		return -EINVAL;
	}
	count = endptr - token;

	if(*endptr != '.' && *endptr != ',')
	{
		*dest_long = num;
		return count;
	}

	token = endptr + 1;	//+1 dot symb (.)
	
	/*
	*	parse digits after dot
	*/
	printk(KERN_DEBUG "in parse double\tberore 2 convert\n");

	numL = simple_strtol(token, &endptr, 10);
	if(endptr == token)
	{
      printk(KERN_DEBUG "ERR in parse double\n");
		return -EINVAL;
	}
	
	count += endptr - token + 1;	//+1 dot symb (.)
		
	if(10 > numL)
	{
		numL *= 10;
	}

	if(num < 0)
	{
		num -= numL;
	}
	else
	{
		num += numL;
	}
	*dest_long = num;
	return count;
}

/*
 * success: return number of characters processed
 * error: return num < 0
 */
static int parse_Currency_name(char dest[4], const char * source)
{
	printk(KERN_DEBUG "in parse curr_name\n");
	
	if(source == NULL || dest == NULL)
	{
	printk(KERN_DEBUG "ERR in parse curr_name\n");
		return -EFAULT;
	}
printk(KERN_DEBUG "in parse curr_name\tlen curr: %zu\n", strlen(source));
	if(strlen(source) < 3)
	{
	printk(KERN_DEBUG "ERR in parse curr_name\n");
		return -EINVAL;
	}
	
	strncpy(dest, source, 3);
	dest[3] = '\0';
	return 3;
}

/* NOTE:
 * str should contain \0
 * success: return 0
 * error: return num < 0
 */
int parse_Exchange_rate(char * str, Exchange_rate_Array * exchange_rates_arr)
{
	struct genradix_iter it;
	Exchange_rate *curr_rate;
	char curr_name[4];
	int res;
	
	if(str == NULL || exchange_rates_arr == NULL)
	{
	printk(KERN_DEBUG "ERR in parse elem\n");
		return -EFAULT;
	}
		
	printk(KERN_DEBUG "in parse elem\tstr: %s\n", str);

	if(str[3] != '=')
	{
	printk(KERN_DEBUG "ERR in parse elem\n");
		return -EINVAL; // str does not match format
	}
	
	res = parse_Currency_name(curr_name, str);
	if(0 > res)
	{
	printk(KERN_DEBUG "ERR in parse elem\n");
		return res;
	}
			
	array_for_each_from(exchange_rates_arr, it, curr_rate, 0)
	{
		if(0 == strcmp(curr_name, curr_rate->currency_name))
		{
			break;
		}
	}
	if(it.pos >= exchange_rates_arr->length)
	{
		curr_rate = genradix_ptr_alloc(&exchange_rates_arr->array,
							exchange_rates_arr->length, GFP_KERNEL);
		if(curr_rate == NULL)
		{
			printk(KERN_DEBUG "ERR in parse arr\n");
			return -1;
		}
		
		++exchange_rates_arr->length;
	}
	strcpy(curr_rate->currency_name, curr_name);

	res = parse_str_double(&curr_rate->convert_factor, str + 4);
	if(0 > res)
	{
	printk(KERN_DEBUG "ERR in parse elem\n");
		return res;
	}

	return 0;
}

/* NOTE:
 * str should contain \0
 * success: return 0
 * error: return num < 0
 */
int parse_exchange_rates(const char * exchange_rates,
		Exchange_rate_Array * exchange_rates_arr)
{
	char ** str = (char **)&exchange_rates;
	char * token;
	int err;

	printk(KERN_DEBUG "in parse arr\tsize: %u\n", strlen(exchange_rates));
	
	if(exchange_rates == NULL || exchange_rates_arr == NULL)
	{
		printk(KERN_DEBUG "ERR in parse arr\n");
		return -EFAULT;
	}

	if(strlen(exchange_rates) < 5)
	{
		printk(KERN_DEBUG "ERR in parse arr\n");
		return -EINVAL;
	}
	
	printk(KERN_DEBUG "in parse arr\t req: %s\n", exchange_rates);

	while((token = strsep(str, ";")))
	{
		printk(KERN_DEBUG "in parse arr\tstrsep len: %u\n", strlen(token));
		if(strlen(token) == 1)
			return 0;

		if(strlen(token) < 5)
		{
			printk(KERN_DEBUG "ERR in parse arr\n");
			return -EINVAL;
		}
			
		err = parse_Exchange_rate(token, exchange_rates_arr);
		
		if(err)
			return err;
		
		if(*str != NULL)
			*(*str-1)=';';
	}
	return 0;

}

/* 
 * Example 1240 set to 12.40
 * success: return number of characters processed
 * error: return num < 0
 */
static int pack_str_double(char * dest_str, size_t str_len, long num)
{
	uint8_t wrote;
	
	if(dest_str == NULL)
	{
      printk(KERN_DEBUG "ERR in pack str_double\n");
		return -EFAULT;
	}
	wrote = snprintf(NULL, 0, "%ld", num) + 3;
	if(wrote > str_len)
	{
		printk(KERN_DEBUG "ERR overflow dest in pack str_double\n");
		return -ENOMEM;
	}
	wrote = sprintf(dest_str, "%ld.", num / 100);
	
	if(10 > (num % 100))
		wrote += sprintf(dest_str + wrote, "0%ld", num % 100);
	else
		wrote += sprintf(dest_str + wrote, "%ld", num % 100);
	
	printk(KERN_DEBUG "in pack str_double\tcoef: %lu\n", num);
	return wrote;
}

/*
 * success: return number of characters processed
 * error: return num < 0
 */
static int pack_Currency_name(char dest[4], size_t str_len, const char * source)
{	
	printk(KERN_DEBUG "in pack curr_name\n");
	
	if(source == NULL || dest == NULL)
	{
		printk(KERN_DEBUG "ERR in pack curr_name\n");
		return -EFAULT;
	}
	if(4 > str_len)
	{
		printk(KERN_DEBUG "ERR in pack curr_name\n");
		return -ENOMEM;
	}	
		printk(KERN_DEBUG "in pack curr_name\tlen curr: %zu\n", strlen(source));
	if(strlen(source) < 3)
	{
		printk(KERN_DEBUG "ERR in pack curr_name\n");
		return -EINVAL;
	}
	
	strncpy(dest, source, 3);
	dest[3] = '\0';
printk(KERN_DEBUG "in pack curr_name\tcurr: %s\n", source);
	return 3;
}

/* NOTE:
 * success: return number of characters processed
 * error: return num < 0
 */
int pack_Exchange_rate(char *str, size_t str_len, Exchange_rate * curr_rate)
{
	char * token = str;
	int res;
	
	if(str == NULL || curr_rate == NULL)
		return -EFAULT;
	
	res = pack_Currency_name(token, str_len, curr_rate->currency_name);
printk(KERN_DEBUG "in pack elem\tcurr: %s\n", curr_rate->currency_name);

	if(0 > res)
	{
printk(KERN_DEBUG "ERR in pack elem\n");
		return res;
	}
	str_len -= res;
	token += res;
	if(str_len < 1)
		return -ENOMEM;
	*token = '=';
	--str_len;
	++token;
	res = pack_str_double(token, str_len, curr_rate->convert_factor);
printk(KERN_DEBUG "in pack elem\tcoef: %ld\n\n", curr_rate->convert_factor);
	if(0 > res)
	{
		printk(KERN_DEBUG "ERR in pack elem\n");
		return res;
	}
	token += res;
	return token - str;
}

/* NOTE:
 * success: return 0
 * error: return num < 0
 */
int pack_exchange_rates(char *str, size_t str_len,
		Exchange_rate_Array * exchange_rates)
{
	struct genradix_iter it;
	Exchange_rate *curr_rate;
	char * token = str;
	int res;
	
	printk(KERN_DEBUG "in pack arr\n");
	if(str == NULL || exchange_rates == NULL)
		return -EFAULT;
	
	array_for_each_from(exchange_rates, it, curr_rate, 0)
	{
		
printk(KERN_DEBUG "in pack arr\tcurr: %s\n", curr_rate->currency_name);
printk(KERN_DEBUG "in pack arr\tcoef: %ld\n\n", curr_rate->convert_factor);
printk(KERN_DEBUG "in pack arr\tstr: %s\n", str);
      
		res = pack_Exchange_rate(token, str_len, curr_rate);
printk(KERN_DEBUG "in pack arr\tcurr: %s\n", curr_rate->currency_name);

		if(0 > res)
		{
	printk(KERN_DEBUG "ERR in pack arr\n");
			return res;
		}
		str_len -= res;
		token += res;
		if(str_len < 1)
			return -ENOMEM;
		*token = ';';
		--str_len;
		++token;
printk(KERN_DEBUG "in pack arr\tcurr: %s\n", curr_rate->currency_name);
printk(KERN_DEBUG "in pack arr\tcoef: %ld\n\n", curr_rate->convert_factor);
	}
	*token = '\0';
	return 0;
}

/* NOTE:
 * str should contain \0
 * success: return 0
 * error: return num < 0
 */
int parse_Exchange_operation(char * str, Exchange_operation * exchange_operation,
		Exchange_rate_Array * exchange_rates)
{
	struct genradix_iter it;
	Exchange_rate *cur_rate;
	char * token = str;
	char cur_name[4];
	int res;

	if(str == NULL || exchange_operation == NULL || exchange_rates == NULL)
	{
		return -EFAULT;
	}

	if(strlen(str) < 9)
	{
	printk(KERN_DEBUG "ERR in parse operat\n");
		return -EINVAL;
	}

	res = parse_Currency_name(cur_name, token);
	if(0 > res)
	{
		printk(KERN_DEBUG "ERR in parse operat\n");
		return res;
	}

	exchange_operation->sell = NULL;
	array_for_each_from(exchange_rates, it, cur_rate, 0)
	{
		if(0 == strcmp(cur_name, cur_rate->currency_name))
		{
			exchange_operation->sell = cur_rate;
		}
	}
	if(exchange_operation->sell == NULL)
	{
		printk(KERN_DEBUG "ERR in parse operat\tsell not found\n");
		return -ENOENT;
	}

	token += res + 1;

	res = parse_str_double(&exchange_operation->sell_amount, token);
	if(0 > res)
	{
		printk(KERN_DEBUG "ERR in parse operat\n");
		return res;
	}

	token += res;
	token = strchr(token, ':');

	if(token == NULL)
	{
		return -EINVAL;
	}
	++token;
	
	res = parse_Currency_name(cur_name, token);
	if(0 > res)
	{
		printk(KERN_DEBUG "ERR in parse operat\n");
		return res;
	}
	
	exchange_operation->buy = NULL;
	array_for_each_from(exchange_rates, it, cur_rate, 0)
	{
		if(0 == strcmp(cur_name, cur_rate->currency_name))
		{
			exchange_operation->buy = cur_rate;
		}
	}
	if(exchange_operation->buy == NULL)
	{
		printk(KERN_DEBUG "ERR in parse operat\tbuy not found\n");
		return -ENOENT;
	}
	
	return 0;
}

/* NOTE:
 * success: return number of characters processed
 * error: return num < 0
 */
int pack_Exchange_operation_ans(char * str, size_t str_len,
			Exchange_operation * exchange_operation)
{
	char * token = str;
	int res;

	if(str == NULL || exchange_operation == NULL)
	{
		return -EFAULT;
	}

	res = pack_Currency_name(token, str_len, exchange_operation->sell->currency_name);
	if(0 > res)
	{
		printk(KERN_DEBUG "ERR in pack operat\n");
		return res;
	}
	
	str_len -= res;
	token += res;
	if(str_len < 1)
		return -ENOMEM;
	*token = ' ';
	--str_len;
	++token;
	
	res = pack_str_double(token, str_len, exchange_operation->sell_amount);
	if(0 > res)
	{
		printk(KERN_DEBUG "ERR in pack operat\n");
		return res;
	}

	str_len -= res;
	token += res;
	if(str_len < 2)
		return -ENOMEM;
	*token = ':';
	++token;
	*token = ' ';
	++token;
	str_len -= 2;
	
	
	res = pack_Currency_name(token, str_len, exchange_operation->buy->currency_name);
	if(0 > res)
	{
		printk(KERN_DEBUG "ERR in pack operat\n");
		return res;
	}
	
	str_len -= res;
	token += res;
	if(str_len < 1)
		return -ENOMEM;
	*token = ' ';
	--str_len;
	++token;
	
	res = pack_str_double(token, str_len, exchange_operation->buy_amount);
	if(0 > res)
	{
		printk(KERN_DEBUG "ERR in pack operat\n");
		return res;
	}
	token += res;
	if(str_len < 1)
		return -ENOMEM;
	*token = '\0';
	--str_len;
	++token;
	
	return token - str;
}
#endif
