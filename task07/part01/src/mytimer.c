/*******************************************************************************
* Copyright (c) 2015 Song Yang @ ittraining
* 
* All rights reserved.
* This program is free to use, but the ban on selling behavior.
* Modify the program must keep all the original text description.
*
* Email: onionys@ittraining.com.tw
* Blog : http://blog.ittraining.com.tw
*******************************************************************************/
#include <linux/module.h>
#include <linux/init.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include <linux/timer.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/init.h>

#define PROC_DIRECTORY  "Mytime"
#define SEC_LAST_READ_FILENAME   "sec_last_read"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ITtraining.com.tw");
MODULE_DESCRIPTION("A timer example.");

struct timer_list my_timer;
static char buffer_read[100];
static u32 jiffies_last_read = 0;


static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file_sec_last;

static ssize_t sec_last_read(struct file *file_p, char __user *buffer,
		size_t length, loff_t *offset);
		
static ssize_t sec_last_write(struct file *file_p, const char __user *buffer,
		size_t length, loff_t *offset);
/*
 * TIMER FUNCTION
 * */

//static void timer_function(unsigned long data){
static void timer_function(struct timer_list * _timer){
	//printk("Time up");
	// modify the timer for next time
	// duration 0.5 sec
	mod_timer(_timer, jiffies + HZ);
}


uint16_t get_hour(u32 second)
{
	return (second / 60 / 60);
}

//max return value is 59
uint16_t get_cur_min(u32 second)
{
	return (second / 60) % 60;
}

//max return value is 59
uint16_t get_cur_sec(u32 second)
{
	return second % 60;
}

//time in format HH:MM:SS
//	hours are set to 00 after 99
char * get_time(u32 second, char buffer [9])
{
	uint16_t temp;

	if((temp = get_hour(second) % 100) < 10) // % 100 for max value is 99
	{
		buffer[0] = '0';
		sprintf(buffer+1, "%d", temp);
	}
	else
	{
		sprintf(buffer, "%d", temp);
	}

	buffer[2] = ':'; //should be here or below (above itoa was set to \0)

	if((temp = get_cur_min(second)) < 10)
	{
		buffer[3] = '0';
		sprintf(buffer+4, "%d", temp);
	}
	else
	{
		sprintf(buffer+3, "%d", temp);
	}

	buffer[5] = ':'; //should be here or below (above itoa was set to \0)

	if((temp = get_cur_sec(second)) < 10)
	{
		buffer[6] = '0';
		sprintf(buffer+7, "%d", temp);
	}
	else
	{
		sprintf(buffer+6, "%d", temp);
	}
	return buffer;
}


static ssize_t sec_last_read(struct file *file_p, char __user *buffer,
		size_t length, loff_t *offset)
{
	size_t left;
	int num_len;
	u32 jiffies_cur;
	u32 jiffies_diff;

	printk(KERN_DEBUG "in read length: %zu\n\n", length);
	jiffies_cur = jiffies;
	if(jiffies_last_read == 0)
	{
		jiffies_diff = 0;
	}
	else
	{
		jiffies_diff = jiffies_cur - jiffies_last_read;
	}
	printk(KERN_DEBUG "in read diff: %ld\n\n", jiffies_diff);
	printk(KERN_DEBUG "in read jiffies: %ld\n\n", jiffies_cur);
	printk(KERN_DEBUG "in read last jiff: %ld\n\n", jiffies_last_read);
	printk(KERN_DEBUG "in read HZ: %ld\n\n", HZ);
	printk(KERN_DEBUG "in read sec: %ld\n\n", jiffies_diff / HZ);
	
	jiffies_last_read = jiffies_cur;
	num_len = sprintf(buffer_read, "%ld\n\n\n", jiffies_diff / HZ);
	printk(KERN_DEBUG "1 in read\tstr_usr: %s\n", buffer_read);
	get_time(jiffies_diff / HZ, buffer_read + num_len);
	printk(KERN_DEBUG "2 in read\tstr_usr: %s\n", buffer_read);
	num_len +=9;
	
	num_len += sprintf(buffer_read+num_len, "\nAbs time\n");
	printk(KERN_DEBUG "3 in read\tstr_usr: %s\n", buffer_read);
	get_time(jiffies_cur / HZ, buffer_read + num_len);
	printk(KERN_DEBUG "4 in read\tstr_usr: %s\n", buffer_read);
	num_len +=9;
	//buffer_read[num_len] = '\0';
	printk(KERN_DEBUG "in read num len: %d\n\n", num_len);
	
	left = raw_copy_to_user(buffer, buffer_read, num_len);
	
	
	if (*offset > num_len)
		*offset = num_len;
	
	if (length > num_len - *offset)
		length = num_len - *offset;
	
	if(length == 0)
		return 0;
	
	*offset = length - left;
	printk(KERN_DEBUG "in read oper\tstr_usr: %s\n", buffer);
	

	if (left)
		printk(KERN_ERR "failed to read oper %u from %u chars\n", left, length);
	else
		printk(KERN_NOTICE "read oper %u chars\n", length);

	return length - left;
}

static ssize_t sec_last_write(struct file *file_p, const char __user *buffer,
		size_t length, loff_t *offset)
{
/*
    size_t left;
    int err;
	printk(KERN_DEBUG "in write oper\tlen: %zu\n", length);
	if (length == 0)
		return 0;
		
    if (length + 1 > BUFFER_SIZE)
    {
printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
        length = BUFFER_SIZE;
    }
		
	left = copy_from_user(exchange_request, buffer, length);
	exchange_request[length] = '\0';
    response_is_relevant = false;

	printk(KERN_DEBUG "in write oper\tstr: %s\n", exchange_request);
	
	err = parse_Exchange_operation(exchange_request, &exchange_operation, &exchange_rates);
	if(0 > err)
	{
		printk(KERN_ERR MODULE_TAG "failed to parse oper %s \n", exchange_request);
		return err;
	}
	conv_log_store(exchange_request);

printk(KERN_DEBUG "in write oper\tcurr: %s\n", genradix_ptr(&exchange_rates.array, 0)->currency_name);
printk(KERN_DEBUG "in write oper\tcoef: %ld\n\n", genradix_ptr(&exchange_rates.array, 0)->convert_factor);
      
    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write oper %u from %u chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %u chars\n", length);

    * */
    return length;
}

static struct file_operations proc_mytime_fops = {
    .read  = sec_last_read,
    .write = sec_last_write
};

static int create_proc_info(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file_sec_last = proc_create(SEC_LAST_READ_FILENAME, S_IFREG | S_IRUGO | S_IWUGO,
						proc_dir, &proc_mytime_fops);
    if (NULL == proc_file_sec_last)
        return -EFAULT;
        

    return 0;
}

static void cleanup_proc_info(void)
{
    if (proc_file_sec_last)
    {
        remove_proc_entry(SEC_LAST_READ_FILENAME, proc_dir);
        proc_file_sec_last = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}
/*
 * INIT MODULE
 * */
int init_module(void)
{
	printk("Hello My Timer\n");


	//  -- initialize the timer
	//timer, callback function, flags
	timer_setup(&my_timer, timer_function, 0);
	// duration 1 sec
	my_timer.expires = jiffies + HZ ;

	// -- TIMER START
	add_timer(&my_timer);
	

	create_proc_info();

	printk("END: init_module() \n");
	return 0;
}

/*
 * CLEANUP MODULE
 * */
void cleanup_module(void)
{
	
	del_timer(&my_timer);
	
	cleanup_proc_info();
	printk("Goodbye\n");
}
