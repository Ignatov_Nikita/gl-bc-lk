
#ifndef PROG_TEST_C
#define PROG_TEST_C
#include <stdio.h>
//#include <string.h>
//#include <fcntl.h>
//#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <alloca.h>
#include <limits.h>

	long long size = 0;
	char * buffer;
	struct timespec time1;
	struct timespec time2;
	int m_fail = 0;
	int c_fail = 0;
	int a_fail = 0;
	clock_t s;
	clock_t e;
	
struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

static void my_malloc(void)
{
	
	s = clock();
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
	
	buffer = malloc(size);
	
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);
	e = clock();
	
	printf("\tall: %ld tick, %ld sec, %ld nsec\n", (e-s), 
			diff(time1,time2).tv_sec, diff(time1,time2).tv_nsec);
	if(!buffer)
	{	
		printf("!!! FAILED, size: %lld \n", size);
		m_fail = 1;
	}
}

static void my_calloc(void)
{
	
	s = clock();
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
	
	buffer = calloc(size, 1);
	
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);
	e = clock();
	
	printf("\tall: %ld tick, %ld sec, %ld nsec\n", (e-s), 
			diff(time1,time2).tv_sec, diff(time1,time2).tv_nsec);
	if(!buffer)
	{	
		printf("!!! FAILED, size: %lld \n", size);
		c_fail = 1;
	}
}

static void my_free(void)
{
	if(buffer)
	{
		s = clock();
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
		
		free(buffer);
		
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);
		e = clock();
		
		printf("\tfree: %ld tick, %ld sec, %ld nsec\n\n", (e-s),
				diff(time1,time2).tv_sec, diff(time1,time2).tv_nsec);
	}
}


static void my_alloca(void)
{
	{
		s = clock();
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);

		char * abuffer = alloca(size);

		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);
		e = clock();

		printf("\tall: %ld tick, %ld sec, %ld nsec\n", (e-s), 
				diff(time1,time2).tv_sec, diff(time1,time2).tv_nsec);

		if(!buffer)
		{
			printf("!!! FAILED, size: %lld \n", size);
			a_fail = 1;
		}
		s = clock();
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
	}
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);
	e = clock();
	printf("\tfree: %ld tick, %ld sec, %ld nsec\n\n", (e-s),
			diff(time1,time2).tv_sec, diff(time1,time2).tv_nsec);
}


static void set_one(int64_t pos)
{
	if(size != 0)
	{
		s = clock();
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
		buffer[pos] = 1;
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);
		e = clock();
		
		printf("\tset 1: %ld tick, %ld sec, %ld nsec\n", (e-s), 
				diff(time1,time2).tv_sec, diff(time1,time2).tv_nsec);
	}
}

int main()
{
	while(1)
	{	
		printf("Buffer size: %lld \n\n", size);
		
		if(!m_fail)
		{
			printf("malloc\n");
			
			//*
			my_malloc();
			my_free();
			//*/
			//*
			printf("\twith set\n\n");
			my_malloc();
			set_one(size - 1);
			my_free();
			//*/
		}
		
		if(!c_fail)
		{
			printf("calloc\n");
			my_calloc();
			my_free();
		}
		
		if(!a_fail)
		{
			/*
			printf("alloca\n");
			my_alloca();
			//*/
		}
		
		if(size == 0)
			++size;
		else
			size*=2;
			
		if(size<0)
			return 0;
			
		if(m_fail && c_fail && a_fail)
			return 0;
	}
	
	return 0;
}
#endif
