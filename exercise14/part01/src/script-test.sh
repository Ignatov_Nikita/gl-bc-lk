#!/bin/bash


module_path='currency_converter.ko'
rates_path='/sys/class/Currency_converter/curr_rates'
exchange_path='/proc/Currency_converter/exchange'
ans=


# param:
# 1 - st1
# 2 - str2
assert_streq()
{
	if [[ $1 != $2 ]]
	then
		printf "\tError - str not equal\n"
		printf "\t$1 != $2\n"
		return 1
	fi
	return 0
}

# param:
# 1 - path file
# 2 - str
write_to_file()
{
	local _path="$1"
	local str="$2"

	printf "$str" > "$_path"
	local err=$?
	if [[ $err -ne 0 ]]
	then
		printf "\tError - writting to file\n"
		return $err
	fi
	return 0
}

# param:
# 1 - path file
# read to variable ans
read_from_file()
{
	local _path="$1"

	ans=$(sudo cat ${_path})
	local err=$?
	if [[ $err -ne 0 ]]
	then
		printf "\tError - reading from file\n"
		return $err
	fi
	return 0
}

# param:
# 1 - str req
# 2 - str rsp
test_rates_rsp_equivalent_req()
{
	sudo rmmod "$module_path" &>/dev/null
	sudo dmesg -c >/dev/null
	sudo insmod "$module_path"

	local req1="$1"
	local rsp1="$2"

	printf "\tWritting to sysfs curr_rates\n"

	write_to_file "$rates_path" "$req1"
	

	printf "\tReading to sysfs curr_rates\n"

	read_from_file "$rates_path"

	assert_streq "$ans" "$rsp1"
	if [[ $? -ne 0 ]]
	then
		printf "[Failed] - test_rates_rsp_equivalent_req\n"
		printf "\t\t bad response curr_rates\n"
	else
		printf "[Passed] - test_rates_rsp_equivalent_req\n"
	fi

	sudo rmmod "$module_path"
}

# param:
# 1 - str req1
# 2 - str req2
# 3 - str rsp
test_rates_rsp_equivalent_req_after_change()
{
	sudo rmmod "$module_path" &> /dev/null
	sudo dmesg -c >/dev/null
	sudo insmod "$module_path"

	local req1="$1"
	local req2="$2"
	local rsp1="$3"

	printf "\tWritting to sysfs curr_rates 1\n"

	write_to_file "$rates_path" "$req1"

	printf "\tWritting to sysfs curr_rates 2\n"

	write_to_file "$rates_path" "$req2"
		
	printf "\tReading to sysfs curr_rates\n"

	read_from_file "$rates_path"

	assert_streq "$ans" "$rsp1"
	if [[ $? -ne 0 ]]
	then
		printf "[Failed] - test_rates_rsp_equivalent_req_after_change\n"
		printf "\t\t bad response curr_rates\n"
	else
		printf "[Passed] - test_rates_rsp_equivalent_req_after_change\n"
	fi

	sudo rmmod "$module_path"
}

# param:
# 1 - str rates
# 2 - str req
# 3 - str rsp
test_operation_rsp_equivalent_req()
{
	sudo rmmod "$module_path" &> /dev/null
	sudo dmesg -c >/dev/null
	sudo insmod "$module_path"

	local rates="$1"
	local req1="$2"
	local rsp1="$3"
	
	printf "\tWritting to sysfs curr_rates\n"

	write_to_file "$rates_path" "$rates"

	printf "\tWritting to procfs exchange\n"

	write_to_file "$exchange_path" "$req1"

	printf "\tReading to procfs exchange\n"

	read_from_file "$exchange_path"

	assert_streq "$ans" "$rsp1"
	if [[ $? -ne 0 ]]
	then
		printf "[Failed] - test_operation_rsp_equivalent_req\n"
		printf "\t\t bad response exchange_operation\n"
	else
		printf "[Passed] - test_operation_rsp_equivalent_req\n"
	fi

	sudo rmmod "$module_path"
}

# param:
# 1 - str rates
# 2 - str req
# 3 - str rsp
test_operation_bad_req_return_err()
{
	sudo rmmod "$module_path" &> /dev/null
	sudo dmesg -c >/dev/null
	sudo insmod "$module_path"

	local rates="$1"
	local req1="$2"
	local rsp1="$3"
	
	printf "\tWritting to sysfs curr_rates\n"

	write_to_file "$rates_path" "$rates"

	printf "\tWritting to procfs exchange\n"

	write_to_file "$exchange_path" "$req1"
	local err=$?
	if [[ $err -eq 0 ]]
	then
		printf "[Failed] - test_operation_bad_req_return_err\n"
		printf "\t\t bad return status exchange_operation: $err \n"
	else
		printf "[Passed] - test_operation_bad_req_return_err: $err\n"
	fi

	printf "\tReading to procfs exchange\n"

	read_from_file "$exchange_path"

	assert_streq "$ans" "$rsp1"
	if [[ $? -ne 0 ]]
	then
		printf "[Failed] - test_operation_bad_req_return_err\n"
		printf "\t\t bad response exchange_operation\n"
	else
		printf "[OK] - test_operation_bad_req_return_err\n"
	fi

	sudo rmmod "$module_path"
}

rates_req1="UAH=1;EUR=30;RUB=0,4"
rates_rsp1="UAH=1.00;EUR=30.00;RUB=0.40;"
op_req1="EUR=93.45:RUB"
op_rsp1="EUR 93.45: RUB 7008.75"

rates_req2="EUR=31;RUB=0.35;USD=28.5"
rates_rsp2="UAH=1.00;EUR=31.00;RUB=0.35;USD=28.50;"
op_req_bad1="EUR=100:USD"
op_rsp_bad1=""
op_req_bad2="EUR=:UAH"
op_rsp_bad2=""
op_req_bad3="E=1:UAH"
op_rsp_bad3=""



printf "START\n"
echo
test_rates_rsp_equivalent_req "$rates_req1" "$rates_rsp1"
echo
echo
test_rates_rsp_equivalent_req_after_change "$rates_req1" "$rates_req2" "$rates_rsp2"
echo
echo
test_operation_rsp_equivalent_req "$rates_req1" "$op_req1" "$op_rsp1"
echo
echo
test_operation_bad_req_return_err "$rates_req1" "$op_req_bad1" "$op_rsp_bad1"
echo
echo
test_operation_bad_req_return_err "$rates_req1" "$op_req_bad2" "$op_rsp_bad2"
echo
echo
test_operation_bad_req_return_err "$rates_req1" "$op_req_bad3" "$op_rsp_bad3"
echo
echo

printf "EXIT\n"

