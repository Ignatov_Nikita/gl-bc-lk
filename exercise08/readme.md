#Ubuntu 7.5.0-3ubuntu1~18.04 build kernel version 5.10.9 

#0 setting dirs
export DIR_INST_SRC_KERNEL=/home/nikitasinys/linux-kernal/
export DIR_INST_SRC_BUILDROOT=/home/nikitasinys/linux-kernal/
export BUILD_KERNEL=/home/nikitasinys/linux-kernal/build_kernel/
export BUILD_ROOTFS=/home/nikitasinys/linux-kernal/build_root/

#1 installing packages
sudo apt-get install qemu-kvm qemu virt-manager virt-viewer libvirt-bin ubuntu-vm-builder bridge-utils
sudo apt-get install libncurses-dev flex bison openssl libssl-dev dkms libelf-dev libudev-dev libpci-dev libiberty-dev autoconf

#2 downloading kernel
cd ${DIR_INST_SRC_KERNEL}
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable -b v5.10.9 --depth 1

export SOURCE_KERNEL=${DIR_INST_SRC_KERNEL}linux-stable/

#3 building kernel
cd ${SOURCE_KERNEL}
make O=${BUILD_KERNEL} i386_defconfig

cd ${BUILD_KERNEL}
make -j4

#4 download buildroot
cd ${DIR_INST_SRC_BUILDROOT}
git clone git://git.buildroot.net/buildroot

export SOURCE_ROOTFS=${DIR_INST_SRC_KERNEL}buildroot/

#5 build builtroot
cd ${SOURCE_ROOTFS}
make O=${BUILD_ROOTFS} qemu_x86_defconfig

cd ${BUILD_ROOTFS}
make menuconfig

echo "user 1000 user 1000 =pass /home/user /bin/bash - Linux User" > ${BUILD_ROOTFS}/users
mkdir -p ${BUILD_ROOTFS}/root/etc/sudoers.d
printf "user\tALL=(ALL) ALL\n" >${BUILD_ROOTFS}/root/etc/sudoers.d/user
echo "/bin/sh" > ${BUILD_ROOTFS}/root/etc/shells
echo "/bin/bash" >> ${BUILD_ROOTFS}/root/etc/shells

make -j4

#6 launching qemu
qemu-system-i386 -kernel ${BUILD_KERNEL}/arch/i386/boot/bzImage -append "root=/dev/sda console=ttyS0" -drive format=raw,file=${BUILD_ROOTFS}/images/rootfs.ext3 -net nic -net user,hostfwd=tcp::8022-:22 &

#7 ssh connecting
ssh -p 8022 user@localhost

cat /proc/version
#(content):
#	Linux version 5.10.9 (nikitasinys@nikitasinys-PCl) (gcc (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0, GNU ld (GNU Binutils for Ubuntu) 2.30) #1 SMP Sun Jan 24 20:05:04 EET 2021


