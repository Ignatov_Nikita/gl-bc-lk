#!/bin/bash

#set default parameters
path="."
recursive=" -maxdepth 1"
delete=" -delete"
allTrue=
DEBUG=

#if possible, get path to dir
if [[ -n "$1" && "$1" != -* ]]
then
	path="$1"
fi

#processing command line parameters
for opt in "$@"
do
	case "$opt" in
	-h | --help)
		echo
		printf "Help information:\n"
		printf "Example: ex1.sh [dir] [options]\n"
		printf "\t-t | --test\n"
		printf "\t\tPrint found elements without deleting\n"
		printf "\t-y | --yes\n"
		printf "\t\tThe answer is yes to all systemic questions\n"
		printf "\t-d | --debug\n"
		printf "\t\tCreates files and directories for test deletion\n"
		printf "\t-r | --recursive\n"
		printf "\t\tSearch in subdirectories\n"
		echo
		exit 0
	;;
	-t | --test)
		delete=
	;;
	-y | --yes)
		allTrue=" -true"
	;;
	-d | --debug)
		DEBUG=1
	;;	
	-r | --recursive)
		recursive=
	;;
	*)
		printf "Unknown parameter: $opt\n"
		exit 1
	;;
	esac
done

#create test file
if [[ $DEBUG -eq 1 ]]
then
	mkdir 1 2 3 ./2/2
	touch 1.tmp 2.tmp ./-1 ~1 _1
	cd 1
	touch 1.tmp 2.tmp ./-1 ~1 _1
	cd ../2
	touch 1.tmp 2.tmp ./-1 ~1 _1 readme.md
	cd ./2
	touch 1.tmp 2.tmp ./-1 ~1 _1
	cd ../../
fi

#find files
res=$(find "$path" ${recursive} -type f \( -name "[_~\-]*" -o -name "*.tmp" \)${allTrue} -print${delete})
if [[ -z "$res" ]]
then
	echo "Files not found"
	exit 1
else
	echo "Found files:"
	printf "${res}\n"
fi

#delete empty dirs
if [[ -z "$recursive" ]]
then
	res=$(dirname $res | uniq)
	find $res -maxdepth 0 ! -name '.' -type d -empty -print${delete}
fi

#view test dirs
if [[ $DEBUG -eq 1 ]]
then
	echo "Content of test dirs:"
	ls ./ ./1 ./2 ./2/2 ./3 --color
fi
