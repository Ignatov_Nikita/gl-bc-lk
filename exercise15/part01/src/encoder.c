#include <linux/init.h>
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/jiffies.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/delay.h>

static const int8_t encoder_state_dir[] = {
  0, -1,  1,  0,
  1,  0,  0, -1,
  -1,  0,  0,  1,
  0,  1, -1,  0,
};

static struct gpio enA;
static struct gpio enB;

static int irq_num;
static int encoder_counter;
static ktime_t last;
static ktime_t debounce_time = 100000;
static int prev_state;
static int cur_state;

inline int encoder_get_counter(void)
{
	return encoder_counter;
}

inline void encoder_set_counter(int _counter)
{
	encoder_counter = _counter;
}

inline ktime_t encoder_get_debounce_time(void)
{
	return debounce_time;
}

inline void encoder_set_debounce_time(ktime_t time)
{
	debounce_time = time;
}


static inline int8_t get_clk(void)
{
	return gpio_get_value(enA.gpio);
}

static inline int8_t get_dt(void)
{
	return gpio_get_value(enB.gpio);
}

static irqreturn_t gpio_isr(int irq, void *data)
{
	static int enc_pos;

	if(ktime_get() - last > 500000000)
	{
		printk(KERN_INFO "\n");
	}
	
	cur_state = (get_clk() << 1) | (get_dt());
	
	if(cur_state != prev_state)
	{		
		enc_pos += encoder_state_dir[(prev_state << 2) | cur_state];
		{
			if(enc_pos == 2)
			{
				enc_pos = 0;
				++encoder_counter;
				printk(KERN_INFO "encoder: turn right: %d deb: %lld, t: %lld\n", 
					encoder_counter, debounce_time, ktime_get() - last);
			}else
			if(enc_pos == -2)
			{
				enc_pos = 0;
				--encoder_counter;
				printk(KERN_INFO "encoder: turn  left: %d deb: %lld, t: %lld\n", 
					encoder_counter, debounce_time, ktime_get() - last);
			}
		}
				 
		last = ktime_get();
		prev_state = cur_state;
	}
	
	return IRQ_HANDLED;
}

int encoder_init(unsigned int _clk, unsigned int _dt, unsigned int _sw)
{
	int ret;

	enA.gpio = _clk;
	enA.flags = GPIOF_IN;
	enA.label = "In Gpio1 encoder";
	enB.gpio = _dt;
	enB.flags = GPIOF_IN;
	enB.label = "In Gpio2 encoder";

	ret = gpio_request(enA.gpio, enA.label);
	if (ret) {
		printk(KERN_ERR "encoder: failed to request GPIO1: %d\n", ret);
		goto err;
        	return -1;
	}
	
	ret = gpio_request(enB.gpio, enB.label);
	if (ret) {
		printk(KERN_ERR "encoder: failed to request GPIO2: %d\n", ret);
		goto err;
        	return -1;
	}
	
	

	ret = gpio_to_irq(enA.gpio);
	if (ret < 0) {
		printk(KERN_ERR "encoder: failed to request IRQ: %d\n", ret);
		goto err;
        	return -2;
	}
	irq_num = ret;
	
	printk(KERN_INFO "encoder: requested IRQ#%d.\n", irq_num);
	
	ret = request_irq(irq_num, gpio_isr, IRQF_TRIGGER_RISING | UMH_DISABLED, "gpio_irqr", NULL);
	if (ret) {
		printk(KERN_ERR "encoder: failed to request IRQ\n");
		goto err;
        	return -3;
	}
	printk(KERN_INFO "encoder: module loaded\n");
	return 0;
	
err:
	free_irq(irq_num,NULL);
	gpio_free(enA.gpio);
	gpio_free(enB.gpio);
	return ret;
}

void encoder_exit(void)
{
	free_irq(irq_num,NULL);
	gpio_free(enA.gpio);
	gpio_free(enB.gpio);
	printk(KERN_INFO "encoder: module exited\n");
}

