
#ifndef _RW_C_
#define _RW_C_

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/init.h>
#include "encoder.c"

MODULE_AUTHOR("Maksym.Lipchanskyi <maxl@meta.ua>");
MODULE_DESCRIPTION("Encoder Demo");
MODULE_INFO(intree, "Y");
MODULE_LICENSE("GPL");


#define MODULE_TAG      "encoder "
#define SYS_DIRECTORY  "Encoder"
#define	EN1_CLK 17
#define	EN1_DT 27
#define	EN1_SW 22

extern inline int encoder_get_counter(void);
extern inline void encoder_set_counter(int _counter);
int encoder_init(unsigned int _clk, unsigned int _dt, unsigned int _sw);
extern void encoder_exit(void);
extern inline ktime_t encoder_get_debounce_time(void);
extern inline void encoder_set_debounce_time(ktime_t time);

static ssize_t counter_show( struct class *class, 
			struct class_attribute *attr, char *buf);
static ssize_t counter_store( struct class *class, 
			struct class_attribute *attr, const char *buf, size_t count);
		
static ssize_t counter_show( struct class *class, 
		struct class_attribute *attr, char *buffer )
{
	ssize_t length;

	printk(KERN_DEBUG "in counter_show length: %zu\n\n", strlen(buffer));
	
	length = sprintf(buffer, "%d\n", encoder_get_counter());

	printk(KERN_DEBUG "in counter_show\tstr_usr: %s\n", buffer);
	
	printk(KERN_NOTICE MODULE_TAG "counter_show %u chars\n", length);

	return length;
}

static ssize_t counter_store( struct class *class,
		struct class_attribute *attr, const char *buffer, size_t length )
{
	int res;
	int counter;

	printk(KERN_DEBUG "in counter_store\n");
	
	res = kstrtoint(buffer, 10, &counter);
	if(res < 0)
	{
		printk(KERN_ERR MODULE_TAG "failed to parse counter %s \n", buffer);
		return 0;
	}
	encoder_set_counter(counter);
		  
	printk(KERN_NOTICE MODULE_TAG "written %d chars\n", length);

	return length;
}

static ssize_t direction_show( struct class *class, 
		struct class_attribute *attr, char *buffer )
{
	ssize_t length;

	printk(KERN_DEBUG "in direction_show length: %zu\n\n", strlen(buffer));
	
	length = sprintf(buffer, "%d\n", encoder_get_counter());

	printk(KERN_DEBUG "in direction_show\tstr_usr: %s\n", buffer);
	
	printk(KERN_NOTICE MODULE_TAG "direction_show %u chars\n", length);

	return length;
}

static ssize_t direction_store( struct class *class,
		struct class_attribute *attr, const char *buffer, size_t length )
{
	int res;
	int counter;

	printk(KERN_DEBUG "in direction_store\n");
	
	res = kstrtoint(buffer, 10, &counter);
	if(res < 0)
	{
		printk(KERN_ERR MODULE_TAG "failed to parse counter %s \n", buffer);
		return 0;
	}
	encoder_set_counter(counter);
		  
	printk(KERN_NOTICE MODULE_TAG "written %d chars\n", length);

	return length;
}

static ssize_t debounce_time_show( struct class *class, 
		struct class_attribute *attr, char *buffer )
{
	ssize_t length;

	printk(KERN_DEBUG "in debounce_time_show length: %zu\n\n", strlen(buffer));
	
	length = sprintf(buffer, "%lld\n", encoder_get_debounce_time());

	printk(KERN_DEBUG "in debounce_time_show\tstr_usr: %s\n", buffer);
	
	printk(KERN_NOTICE MODULE_TAG "read debounce_time_show %u chars\n", length);

	return length;
}

static ssize_t debounce_time_store( struct class *class,
		struct class_attribute *attr, const char *buffer, size_t length )
{
	int res;
	ktime_t time;

	printk(KERN_DEBUG "in debounce_time_store\n");
	
	res = kstrtos64(buffer, 10, &time);
	if(res < 0)
	{
		printk(KERN_ERR MODULE_TAG "failed to parse counter %s \n", buffer);
		return 0;
	}
	encoder_set_debounce_time(time);
		  
	printk(KERN_NOTICE MODULE_TAG "written %d chars\n", length);

	return length;
}

CLASS_ATTR_RW( debounce_time );
CLASS_ATTR_RW( counter );
CLASS_ATTR_RW( direction );
static struct class *encoder_class;


static int create_sys_info(void)
{
	int err;
	
	encoder_class = class_create( THIS_MODULE, SYS_DIRECTORY );
	if( IS_ERR( encoder_class ) ) 
		printk( "bad class create\n" );
	err = class_create_file( encoder_class, &class_attr_counter );
	if (err)
		return -EFAULT;
	err = class_create_file( encoder_class, &class_attr_direction );
	if (err)
		return -EFAULT;
	err = class_create_file( encoder_class, &class_attr_debounce_time );
	if (err)
		return -EFAULT;
		
    return 0;
}

static void cleanup_sys_info(void)
{
	class_remove_file( encoder_class, &class_attr_counter );
	class_remove_file( encoder_class, &class_attr_direction );
	class_remove_file( encoder_class, &class_attr_debounce_time );
	class_destroy( encoder_class );
}

//*
static int __init rw_init(void)
{
    int err;

	printk(KERN_NOTICE MODULE_TAG "Converter start\n");
    err = encoder_init(EN1_CLK, EN1_DT, EN1_SW);
	if (err)
		goto error;
        
	printk(KERN_NOTICE MODULE_TAG "bef sys-class\n");
        
	err = create_sys_info();
	if (err)
		goto error;
        
    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_sys_info();
    encoder_exit();
    return err;
}


static void __exit rw_exit(void)
{
    cleanup_sys_info();
    encoder_exit();
    printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(rw_init);
module_exit(rw_exit);
//*/
#endif
